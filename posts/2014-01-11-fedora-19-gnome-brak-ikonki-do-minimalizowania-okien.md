---
title: 'fedora 19: gnome brak ikonki do minimalizowania okien'
author: szydell
type: post
date: 2014-01-11T09:19:35+00:00
url: /post/2014/fedora-19-gnome-brak-ikonki-do-minimalizowania-okien/
categories:
  - Linux
tags:
  - bash
  - Fedora
  - Fedora 19
  - gnome
  - gnome-tweak-tool

---
Dziwne&#8230; standardowo okno można zamknąć, bądź też zmaksymalizować (dwuklik na belce tytułowej)

<pre># yum install gnome-tweak-tool
$ gnome-tweak-tool
</pre>

Opcja którą szukamy to:  
Powłoka -> uporządkowanie przycisków na pasku tytułowym

Fakt, że znikanie okien też jest jakieś takie do dupy, bo nie widać ich po zniknięciu na żadnym pasku&#8230; no ale, lepsze to niż nic.

<pre>yum install gnome-shell-extension-window-list
</pre>