---
title: GT.M + AJAX. M Gateway. EWD Project!
author: szydell
type: post
date: 2016-01-01T20:19:36+00:00
url: /post/2016/gt-m-ajax-m-gateway-ewd-project/
categories:
  - gt.m
tags:
  - ajax
  - ewd
  - gt.m
  - m gateway
  - python

---
Bardzo długo szukałem tego projektu. Ciekawe rozwiązanie.

<a href="https://github.com/robtweed/EWD" target="_blank">https://github.com/robtweed/EWD</a>

W skrócie &#8211; framework do tworzenia stron www z użyciem bazy gt.m  
Ważne plusy &#8211; moduł (uniwersalny) dający w pythonie możliwość manipulowania globalami na bazie.

&nbsp;