---
title: 'OpenVMS: tcpdump przykłady'
author: szydell
type: post
date: 2014-01-08T09:44:53+00:00
url: /post/2014/openvms-tcpdump-przyklady/
categories:
  - OpenVMS
tags:
  - Alpha
  - OpenVMS
  - tcpdump

---
Uwaga! Użycie tcpdumpa może wymagać podniesienia uprawnień:

<pre>set process/privileges=all</pre>

&nbsp;

Proste przykłady:

Wyświetl cały ruch na porcie 23, wyświetl cały ruch na porcie 35 lub 78

<pre>tcpdump port 23
tcpdump port 35 or port 78</pre>

&nbsp;

Wyświetl ruch TCP na porcie telnet

<pre>tcpdump tcp port telnet</pre>

&nbsp;

Wyświetl cały ruch pochodzący z (idący do) hosta 192.168.0.1

<pre>tcpdump src 192.168.0.1
tcpdump dst 192.168.0.1
tcpdump src or dst 192.168.0.1</pre>

&nbsp;