---
title: selinux, a wordpress
author: szydell
type: post
date: 2019-09-21T21:43:20+00:00
url: /post/2019/selinux-a-wordpress/
categories:
  - Linux
tags:
  - SELinux
  - wordpress

---
Na maszynach z selinuxem może dojść do problemów z zapisywaniem przez wordpressa. Rozwiązanie?

<pre class="wp-block-code"><code>semanage fcontext -a -t httpd_sys_rw_content_t "/&lt;ścieżka>/wp-content(/.*)?"
semanage fcontext -a -t httpd_sys_rw_content_t "/&lt;ścieżka>/.htaccess"
restorecon -r &lt;ścieżka>
</code></pre>