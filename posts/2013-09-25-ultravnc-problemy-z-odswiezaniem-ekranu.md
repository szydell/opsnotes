---
title: UltraVNC – problemy z odświeżaniem ekranu
author: szydell
type: post
date: 2013-09-25T07:38:42+00:00
url: /post/2013/ultravnc-problemy-z-odswiezaniem-ekranu/
categories:
  - VNC
tags:
  - .ini
  - UltraVNC
  - VNC

---
UltraVNC potrafi w niektórych przypadkach nie zadziałać Out Of Box w pełni poprawnie. Jednym z problemów, na które natrafiłem było strasznie wolne odświeżanie ekranu klienckiego.

Na szczęście rozwiązanie jest bardzo proste i polega na poprawnym skonfigurowaniu klienta UltraVNC. W pliku UltraVNC.INI musimy mieć ustawione następujące opcje:

[poll]  
TurboMode=1  
PollUnderCursor=0  
PollForeground=1  
PollFullScreen=1  
OnlyPollConsole=0  
OnlyPollOnEvent=0  
EnableDriver=1  
EnableHook=1  
EnableVirtual=0  
SingleWindow=0  
SingleWindowName=

Dodatkowo, jeżeli uruchamiasz klienta na Windowsie XP w niektórych przypadkach trzeba również załadować driver video.

Rozwiązanie znalazłem na stronie: <a title="UltraVNC Refresh Issue" href="http://www.itninja.com/question/ultravnc-refresh-issue?from=appdeploy.com" target="_blank">http://www.itninja.com/question/ultravnc-refresh-issue?from=appdeploy.com</a>