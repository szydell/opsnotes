---
title: 'MUSH/Arkadia: plugin dodajacy info o aktualnie panującej porze roku.'
author: szydell
type: post
date: 2014-01-06T23:20:37+00:00
url: /post/2014/musharkadia-plugin-dodajacy-info-o-aktualnie-panujacej-porze-roku/
categories:
  - MUSH Client
tags:
  - Arkadia
  - kalendarz
  - MUSH Client
  - plugin

---
Z pluginem:

<pre>czas
Jest w przyblizeniu druga po poludniu, trzynasty dzien pory Blathe wedlug rachuby czasu Starszego Ludu. (Pozna wiosna).
</pre>

Bez pluginu:

<pre>czas
Jest w przyblizeniu druga po poludniu, trzynasty dzien pory Blathe wedlug rachuby czasu Starszego Ludu.
</pre>

Pobrać do katalogu z pluginami, rozpakować (7zip), zainstalować w MUSH Cliencie.

Ssij: [PoryRoku.7z][1]

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2014/01/PoryRoku.7z