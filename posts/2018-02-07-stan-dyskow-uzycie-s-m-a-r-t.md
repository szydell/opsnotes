---
title: Stan dysków, użycie S.M.A.R.T.
author: szydell
type: post
date: 2018-02-07T08:52:15+00:00
url: /post/2018/stan-dyskow-uzycie-s-m-a-r-t/
categories:
  - Aplikacje

---
Szybka ściągawka:

  * Windows 7, Command Prompt:  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">wmic diskdrive get status</pre>

  * Linux  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">sudo smartctl -a /dev/sda
sudo smartctl -t short /dev/sda
sudo smartctl -l selftest /dev/sda</pre>