---
title: 'DA: Inkwizycja + TeamViewer = Problem z wyświetlaniem w trybie Full Screen'
author: szydell
type: post
date: 2015-01-31T11:56:27+00:00
url: /post/2015/da-inkwizycja-teamviewer-problem-z-wyswietlaniem-w-trybie-full-screen/
categories:
  - Aplikacje
tags:
  - DA:I
  - 'Dragon Age: Inkwizycja'
  - 'Dragon Age: Inquisition'
  - Full screen
  - Full Screen FIX
  - TeamViewer

---
Dragon Age: Inquisition nie lubi się z TeamViewerem 🙂  
W sieci jako rozwiązanie podawane jest całkowite wyłączenie TeamViewera na czas grania w DA:I. Nie ma takiej potrzeby.

Wystarczy w TeamViewerze wyklikać:

  1. Dodatki
  2. Opcje
  3. Zaawansowane
  4. Pokaż opcje zaawansowane
  5. Przycisk SzybkiePołączenie -> Konfiguruj
  6. DragonAgeInquisition.exe <dodaj></dodaj>

I po problemie. Przyczyną niepoprawnego działania DA:I w trybie Full Screen, nie jest TeamVeawer jako taki, a jedynie przycisk, który standardowo dokleja się do górnej belki okna i pozwala szybko udostępnić komuś obraz. Nie ma przycisku &#8211; Nie ma problemu.