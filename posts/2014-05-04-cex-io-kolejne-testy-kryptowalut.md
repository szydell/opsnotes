---
title: CEX.IO – Kolejne testy kryptowalut
author: szydell
type: post
date: 2014-05-04T21:50:36+00:00
url: /post/2014/cex-io-kolejne-testy-kryptowalut/
categories:
  - Kryptowaluty
tags:
  - BITCOIN
  - GHZ
  - kopalnia
  - koparka

---
Sprawa jest prosta. Budowa koparki to koszty. Jej utrzymanie też.

A co będzie jeżeli wynajmiemy moc obliczeniową?

Zaczynam testy:  
<a href="https://cex.io/r/0/szydell/0/" title="CEX.IO - Trade Ghashes while they mine you Bitcoins!" target="_blank"><img loading="lazy" decoding="async" src="http://cex.io/informer-small/szydell/7cfd096375ed96fe099c94a685406bd3/" width="430" height="30" border="0" /></a>

Śmiało się rejestrujcie. Oczywiście coś tam z tego będę miał, ale nie zmienia to faktu, że Wy również 🙂