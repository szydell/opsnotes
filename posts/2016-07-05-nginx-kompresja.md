---
title: NGINX kompresja
author: szydell
type: post
date: 2016-07-05T19:26:26+00:00
url: /post/2016/nginx-kompresja/
categories:
  - nginx
tags:
  - gzip
  - kompresja
  - nginx
  - pagespeed

---
Google udostępnia całkiem sympatyczne narzędzie do analizowania jakości strony &#8211; [PageSpeed][1]. Ciągle dostawałem tam informację, że moje serwery nie kompresują zawartości. Kombinowałem i kombinowałem&#8230;  
Okazało się, że w 'gzip_types&#8217; brakowało typu 'application/javascript&#8217;. W dokumentacji nginx&#8217;a tego nie ma&#8230;

Cały wpis odpowiadający za włączenie kompresji w nginx&#8217;ie:

<?prettify linenums=true?>

<pre class="prettyprint">gzip on;
gzip_comp_level 2;
gzip_http_version 1.0;
gzip_proxied any;
gzip_min_length 1100;
gzip_buffers 16 8k;
gzip_types text/plain text/html text/css application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
gzip_disable "MSIE [1-6].(?!.*SV1)";
gzip_vary on;</pre>

 [1]: https://developers.google.com/speed/pagespeed/insights/