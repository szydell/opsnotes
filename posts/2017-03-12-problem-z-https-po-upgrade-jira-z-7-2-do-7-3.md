---
title: Problem z HTTPS po upgrade JIRA z 7.2 do 7.3
author: szydell
type: post
date: 2017-03-12T14:54:22+00:00
url: /post/2017/problem-z-https-po-upgrade-jira-z-7-2-do-7-3/
categories:
  - JIRA
  - nginx
  - tomcat
tags:
  - https
  - JIRA
  - JIRA 7.3
  - nginx
  - ssl
  - tomcat

---
W mojej konfiguracji używam JIRA za proxy postawionym na nginxie. Po upgrade do wersji 7.3 (a co za tym idzie też podniesieniu wersji tomcata do wersji 8.5) JIRA przestała działać z ustawionym parametrem connectora:

<?prettify linenums=true?>

<pre class="prettyprint">proxyName="jira.example.com" proxyPort="443" scheme="https" secure="true"</pre>

Jak to zwykle bywa okazało się, że to [known issue][1] 😉 Jak to jeszcze częściej bywa known issue nie wprost odwołuje się do tego co miałem w konfigu, ale workaround zadziałał.

Rozwiązanie problemu to zmiana parametru protocol na:  
<?prettify linenums=true?>

<pre class="prettyprint">protocol="org.apache.coyote.http11.Http11NioProtocol"</pre>

 [1]: https://jira.atlassian.com/browse/JRA-63734?focusedCommentId=1005958&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-1005958