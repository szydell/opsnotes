---
title: 'MUSH/Arkadia: Plugin zwracający do MUDa informację o stanie łącza'
author: szydell
type: post
date: 2014-01-04T22:46:32+00:00
url: /post/2014/musharkadia-plugin-zwracajacy-do-muda-informacje-o-stanie-lacza/
categories:
  - MUSH Client
tags:
  - Arkadia
  - MUD
  - MUSH Client

---
Na MUDzie Arkadia pojawiła się niedawno funkcjonalność pozwalająca wymuszać 'utrzymanie łącza&#8217;.  
Od strony muda uruchamia się ją komendą:

<pre>$ opcje utrzymywanie obustronne
UTRZYMYWANIE polaczenia:                Obustronne</pre>

W załączniku pliczek implementujący tę funkcjonalność po stronie MUSH Clienta.

Ssij: [utrzymywanie.7z][1]

<small>(Plik spakowany 7zipem. Należy rozpakować do folderu z pluginami i 'zainstalować&#8217; plugin w MUSH Cliencie)</small>

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2014/01/utrzymywanie.7z