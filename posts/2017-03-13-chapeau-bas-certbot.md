---
title: Chapeau bas Certbot!
author: szydell
type: post
date: 2017-03-13T19:42:17+00:00
url: /post/2017/chapeau-bas-certbot/
categories:
  - nginx
tags:
  - Apache
  - certbot
  - certbot-auto
  - darmowe
  - free
  - https
  - ssl

---
Skończył mi się certyfikat SSL dla tego bloga. StartSSL podpadł wszystkim dookoła, trzeba było zrobić z tym porządek.  
Z pomocą przyszła inicjatywa <a href="https://letsencrypt.org/getting-started/" target="_blank">Let&#8217;s Encrypt</a>, Electronic Frontier Foundation i rewelacyjny skrypt certbot 🙂

Co zrobiłem, żeby mieć nowy certyfikat:  
`<br />
# cd /opt<br />
# mkdir certbot<br />
# chmod 700 certbot<br />
# cd certbot/<br />
# wget https://dl.eff.org/certbot-auto<br />
# chmod 700 certbot-auto<br />
# ./certbot-auto --nginx<br />
`  
Certbot wyświetlił mi wszystkie skonfigurowane na nginxie domeny i poprosił o wybranie tej odpowiedniej.  
Podałem maila, zgodziłem się na otrzymanie wiadomości w momencie gdy certyfikat się będzie miał ku końcowi.  
Certbot poprawił config dla podanej domeny, wygenerował podpisany przez Let&#8217;s Encrypt certyfikat, zrobił reload nginxa.  
Koniec.

**Wow?!**

PS  
Certbot dostępny jest też w repozytorium EPEL ale jest tam starsza wersja, która jeszcze nie kuma nginxa. Za to dla fanów Apache &#8211; polecam 🙂

PS2  
Pisałem już, że certbot i generowane przez Let&#8217;s Encrypt **certyfikaty są darmowe**?

PS3  
Co do jakości samego certyfikatu:  
 <a href="https://www.ssllabs.com/ssltest/analyze.html?d=marcin.szydelscy.pl" target="_blank">https://www.ssllabs.com/ssltest/analyze.html?d=marcin.szydelscy.pl</a>