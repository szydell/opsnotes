---
title: setfacl czyli ACLe w akcji. Dostęp do logów.
author: szydell
type: post
date: 2016-01-07T21:04:12+00:00
url: /post/2016/acle-w-akcji-dostep-do-logow/
categories:
  - Linux
tags:
  - ACL
  - getfacl
  - log
  - nginx
  - permissions
  - setfacl

---
Setfacl to jedno z narzędzi do zarządzania ACL&#8217;ami pod Linuxem. Notatka praktyczna pokazująca jak można je zastosować w realnym świecie.

Czasami istnieje konieczność przyznania komuś dostępu do logów. Dostępu stałego, uwzględniającego <a href="https://www.redhat.com/sysadmin/setting-logrotate" target="_blank" rel="noopener">logrotate</a> i ewentualne fanaberie niektórych aplikacji (zmień ownera po stworzeniu pliku &#8211; patrz Apache).

Najskuteczniejszą moim zdaniem metodą jest skorzystanie z ACL&#8217;a nałożonego na katalog.  
Przykładowo. Chcemy dodać członkom grupy _www_ dostęp read do logów nginxa:  
<!--?prettify linenums=true?-->

<pre class="prettyprint">setfacl -d -m g:www:r nginx/    # to załatwia sprawę dla przyszłych plików (można też pojechać rekurencyjnie -R)</pre>

wydanie komendy spodowowało:  
<!--?prettify linenums=true?-->

<pre class="prettyprint">[root@linux log]# getfacl nginx/
# file: nginx/
# owner: root
# group: root
user::rwx
group::r-x
other::r-x
default:user::rwx
default:group::r-x
default:group:www:r--
default:mask::r-x
default:other::r-x</pre>

<!--?prettify linenums=true?-->

<pre class="prettyprint">setfacl -m g:www:r nginx/* # a tak dorzucamy +r do wszystkich obecnych.</pre>

Jeżeli uprawnienia miałyby być nadane nie na grupę tylko konkretnego usera to:  
<!--?prettify linenums=true?-->

<pre class="prettyprint">setfacl -d -m u:login:r nginx/</pre>

Usunięcie acla:  
<!--?prettify linenums=true?-->

<pre class="prettyprint">setfacl -x g:www nginx/
setfacl -x u:login nginx/</pre>