---
title: 'OpenVMS: limit ilości wersji danego pliku'
author: szydell
type: post
date: 2014-01-24T11:53:21+00:00
url: /post/2014/openvms-limit-ilosci-wersji-danego-pliku/
categories:
  - OpenVMS
tags:
  - /before
  - Alpha
  - del
  - delete
  - OpenVMS
  - rename
  - wersjonowanie

---
OpenVMS posiada wbudowany w filesystem świetny patent &#8211; wersjonowanie plików.  
Niestety ma on jedno drobne ograniczenie &#8211; trzymać można maksymalnie do **32767** wersji.

W momencie gdy dojedziemy do maksymalnego numeru wersji, filesystem nie pozwoli nagrać nowego pliku o danej nazwie.

Jak się przed tym bronić?

Oczywiście kasować! Ale nie tylko. Skasowanie najstarszych wersji, bez 'przewinięcia&#8217; licznika nic tutaj nie zmieni. System nie sprawdza realnej ilości plików, tylko cyferkę przy wersji.

Przykład:  
Żeby skasować pliki starsze niż 30 dni i przewinąć taki licznik, trzeba skorzystać z komendy 'delete /before&#8217; i 'rename&#8217;:

<pre>$ delete filename.ext;* /before=-30-0
$ RENAME filename.ext;* TEMPORARY.TMP;
$ RENAME TEMPORARY.TMP;* filename.ext;</pre>

Hint wykombinowany z pomocą: <a title="http://labs.hoffmanlabs.com/node/456" href="http://labs.hoffmanlabs.com/node/456" target="_blank">http://labs.hoffmanlabs.com/node/456</a>