---
title: SSH długi czas logowania
author: szydell
type: post
date: 2021-07-11T21:00:21+00:00
url: /post/2021/ssh-dlugi-czas-logowania/
featured_image: /wp-content/uploads/2021/07/ssh-350x270.jpg
categories:
  - Fedora
  - Linux
tags:
  - logrotate
  - ssh
  - sshd

---
**GSS** &#8211; Przy próbie połączenia się po ssh, dodaj -vvv do polecenia. Odpalisz w ten sposób szczegółowy debug. Jeżeli w logu zatrzymujesz się przy wpisach podobnych do tych:

<pre class="wp-block-preformatted">debug1: Unspecified GSS failure. &nbsp;Minor code may provide more information 
Ticket expired</pre>

Rozwiązaniem jest zedytowanie pliku /etc/ssh/sshd_config i ustawienie:

<pre id="output" class="wp-block-preformatted">GSSAPIAuthentication no</pre>

Jeżeli pracujesz na Fedorze/RHEL/CentOS&#8217;ie, to ustawienie to bywa zdublowane w pliku /etc/ssh/sshd_config.d/50-redhat.conf. Również w nim zmień wartość na no.

**<a href="https://pl.wikipedia.org/wiki/Reverse_DNS" target="_blank" rel="noreferrer noopener">RevDNS</a>** &#8211; Zdarza się, że opóźnienie powodowane jest sprawdzaniem nazwy hosta, z którego się łączysz. Żeby wyłączyć to sprawdzanie ustaw:

<pre class="wp-block-preformatted">UseDNS no</pre>

W celu zatwierdzenia zmian, zrestartuj serwer sshd. Na Fedorze zrobisz to bezpiecznie komendą:

<pre class="wp-block-preformatted"># systemctl restart sshd</pre>

**btmp** &#8211; powyższe nie pomogło? Sprawdź jak duży jest twój plik /var/log/btmp.  
Czemu? Przykładowo na Fedorach, w `/etc/pam.d/postlogin` skonfigurowane jest:

<pre class="wp-block-preformatted"><code>session     optional      pam_lastlog.so silent noupdate showfailed</code></pre>

Opcja showfailed, sprawdza w btmp ile było nieprawidłowych prób zalogowania się na Twój login. Jeżeli serwer jest publiczny, to zapewne nie było ich mało. Rozwiązanie?  
Skasuj btmp, zainstaluj logrotate i skonfiguruj częstsze czyszczenie tego logu.  
Na Fedorze będzie wyglądać to tak. 

<pre class="wp-block-preformatted"># dnf install logrotate
# systemctl enable logrotate.timer</pre>

Logrotate standardowo czyści btmp. Definicję znajdziesz w pliku _/etc/logrotate.d/btmp_. Dzięki włączeniu timera, logrotate uruchomi się raz dziennie.