---
title: Zmiana UUID partycji
author: szydell
type: post
date: 2017-10-28T20:13:57+00:00
url: /post/2017/zmiana-uuid-partycji/
categories:
  - Linux
tags:
  - CentOS
  - Debian
  - Red Hat Enterprise Linux
  - tune2fs
  - Ubuntu
  - uuid

---
Dysk HDD na którym miałem system okazał się być zbyt wolny, także sklonowałem jego zawartość na SSD używając komendy 'dd&#8217;. Teraz chciałbym, żeby UUIDy na nowym dysku były wygenerowane od nowa. Jak to zrobić?

Komendą 'uuidgen&#8217; generujemy nowy UUID:  
<?prettify linenums=true?>

<pre class="prettyprint">[szydell@grabki ~]$ uuidgen
5e0b0314-fe0c-4db8-8908-c815d0b67e1f</pre>

Na CentOS/Red Hat 7 narzędzie to dostępne jest w pakiecie 'linux-utils&#8217;. Na Debianie i Ubuntu trzeba doinstalować pakiet 'uuid-runtime&#8217;.

Następnie zmieniamy UUID na wybranej przez nas partycji używając 'tune2fs -U&#8217;

<?prettify linenums=true?>

<pre class="prettyprint">sudo tune2fs /dev/sda1 -U 5e0b0314-fe0c-4db8-8908-c815d0b67e1f</pre>