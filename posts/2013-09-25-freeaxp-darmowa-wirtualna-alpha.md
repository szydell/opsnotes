---
title: FreeAXP™ – Darmowa wirtualna Alpha
author: szydell
type: post
date: 2013-09-25T12:54:53+00:00
url: /post/2013/freeaxp-darmowa-wirtualna-alpha/
categories:
  - Alpha
  - OpenVMS
tags:
  - Alpha
  - Digital UNIX
  - emulator
  - FreeAXP
  - OpenVMS
  - Tru64 Unix
  - UNIX
  - windows

---
Emulator, dzięki któremu można na x86 zasymulować komputer z procesorem Alpha.

Do ściągnięcia ze strony: <a title="FreeAXP" href="http://www.migrationspecialties.com/FreeAXP.html" target="_blank">http://www.migrationspecialties.com/FreeAXP.html</a>

Niezbędne rozwiązanie dla osób, które nie mając dostępu do sprzętu chciałyby zająć się np. nauką systemu OpenVMS.

FreeAXP w skrócie:

  * Darmowy do zastosowań komercyjnych i prywatnych
  * Brak konieczności rejestrowania
  * **Wirtualne środowisko w którym można zainstalować OpenVMS i Tru64 UNIX**
  * Wsparcie dla Windowsów 32 i 64bit
  * Prosta konfiguracja
  * Pracuje także na maszynach wirtualnych (vmware->windows->FreeAxp)

Dostępne komponenty 'sprzętowe&#8217;:

  * 32 &#8211; 128MB wirtualnego ramu
  * do siedmiu dysków twardych
  * Jeden fizyczny CD ROM
  * Dwie karty sieciowe

Obsługiwane systemy operacyjne (do uruchomienia wewnątrz emulatora)

  * OpenVMS 6.2
  * OpenVMS 6.2-1H3
  * OpenVMS 7.0
  * OpenVMS 7.1
  * OpenVMS 7.1-1H1
  * OpenVMS 7.2
  * OpenVMS 7.2-1
  * OpenVMS 7.3
  * OpenVMS 7.3-1
  * OpenVMS 7.3-2
  * OpenVMS 8.2
  * OpenVMS 8.3
  * OpenVMS 8.4
  * Digital UNIX V3.2C
  * Digital UNIX V3.2G
  * Digital UNIX V4.0B
  * Digital UNIX V4.0D
  * Digital UNIX V4.0E
  * Digital UNIX V4.0F
  * Digital UNIX V4.0G
  * Tru64 UNIX V5.0
  * Tru64 UNIX V5.0A
  * Tru64 UNIX V5.1
  * Tru64 UNIX V5.1A
  * Tru64 UNIX V5.1B
  * Tru64 UNIX V5.1B-5
  * Tru64 UNIX V5.1B-6

&nbsp;