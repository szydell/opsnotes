---
title: Problemy z pendrive pod windows 10
author: szydell
type: post
date: 2016-04-29T15:04:53+00:00
url: /post/2016/problemy-z-pendrive-pod-windows-10/
categories:
  - Windows
tags:
  - diskpart
  - menedżer dysków wirtualnych
  - nie można odnaleźć określonego pliku
  - pendrive
  - windows 10

---
Po repartycjonowaniu pendrive nie mogłem z niego korzystać. Partycjonowanie przez 'Zarządzanie dyskami&#8217; nie działa &#8211; zwraca wynik: 'menedżer dysków wirtualnych nie można odnaleźć określonego pliku&#8217;, ale zadziałało to:

<?prettify linenums=true?>

<pre class="prettyprint">diskpart
list disk     #pokaże się lista dysków
select disk X #X - nr z listy
clean
create partition primary
select partition 1
format quick fs=fat32
assign
exit</pre>