---
title: '[GIT] Jednoczesny push do wielu repozytoriów'
author: szydell
type: post
date: 2019-04-10T07:53:55+00:00
url: /post/2019/git-jednoczesny-push-do-wielu-repozytoriow/
categories:
  - GIT
tags:
  - push
  - repozytorium

---
Czasami dobrze jest trzymać projekt w wielu miejscach. Od paru lat git pozwala na wskazanie docelowych repozytoriów dla pushów:  


<pre class="wp-block-code"><code>git remote set-url origin --push --add &lt;....></code></pre>

Dodajemy w ten sposób wszystkie repozytoria do których ma trafić nasz kod. I teraz jednym 'git push&#8217; wysyłamy wszędzie.