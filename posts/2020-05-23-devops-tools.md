---
title: devops tools
author: szydell
type: post
date: 2020-05-23T14:07:03+00:00
url: /post/2020/devops-tools/
categories:
  - Aplikacje
  - DevOps
tags:
  - xenialabs

---
<blockquote class="wp-block-quote">
  <p>
    <a rel="noreferrer noopener" href="https://xebialabs.com/periodic-table-of-devops-tools/" target="_blank">Periodic Table of DevOps Tools</a>
  </p>
  
  <cite>Wiedza i efekt wow w jednym.</cite>
</blockquote>

Genialne zestawienie &#8211; 'Tablica okresowa narzędzi DevOps&#8217;.  
  
Teraz też z możliwością przygotowania wizualizacji własnego stosu technologicznego.

 