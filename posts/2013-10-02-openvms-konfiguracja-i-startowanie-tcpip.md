---
title: 'OpenVMS: Konfiguracja i startowanie TCP/IP'
author: szydell
type: post
date: 2013-10-02T11:34:35+00:00
url: /post/2013/openvms-konfiguracja-i-startowanie-tcpip/
categories:
  - OpenVMS
tags:
  - Alpha
  - konfiguracja
  - OpenVMS
  - TCP/IP

---
Do konfigurowania różnych elementów OpenVMS&#8217;a używa się kreatorów. Wszelkie kwestie związane z TCP/IP ustalamy z kreatora:

<pre>$ @SYS$MANAGER:TCPIP$CONFIG.COM</pre>

Z poziomu konfiguratora można zmienić parametry na 'żywym systemie&#8217;, można też wyłączyć i włączyć wszystkie usługi związane z TCP/IP.

Alternatywnie włączenie 'sieci&#8217; można zrealizować przez wykonanie komendy:

<pre>@SYS$STARTUP:TCPIP$STARTUP</pre>

Niegłupim pomysłem jest dopisanie jej do skryptów startowych. VMS 'out of box&#8217; nie uruchamia TCP/IP przy starcie.