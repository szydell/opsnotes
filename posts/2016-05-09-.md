---
title: Odpytywanie po MTM + gdzie leżą logi.
author: szydell
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=341
categories:
  - Aplikacje

---
Aby uzyskać dostęp do usług, przy użyciu sterownika JDBC:

<pre>public static void main (String args[]) {
        
            try {
                        
            // Load the jdbc driver
            Class.forName ("fisglobal.jdbc.driver.ScDriver");
            
                  // Sanchez JDBC driver support two URL syntax
                  // String url = "protocol=jdbc:sanchez/database=10.166.129.47:19417:SCA$IBS/locale=US:ENGLISH/timeOut=2/transType=MTM/rowPrefetch=30/signOnType=1";
            String url = "protocol=jdbc:fisglobal/database=255.16.84.16:19300:SCA$IBS/locale=US:ENGLISH/timeOut=2/transType=MTM/rowPrefetch=30/signOnType=1";
                  Connection con = DriverManager.getConnection (url, "1", "xxx");
 
 
            CallableStatement cstatement;
            String mrpc = "29";                                                ID MRPC, zdefiniowanego i skonfigurowanego
            String inputParameters = "par1,par2,par3,par4";                      kolejne parametry (wartości) wejściowe MRPC, z pomnięciem version i RETURN
            ResultSet results;
            String response;
 
            
            final String mrpcString = "{call mrpc(?, ?, ?, ?)}";               ilość znkaów ? to kolejne parametry wejściowe MRPC, z pomnięciem version i RETURN
            cstatement = con.prepareCall(mrpcString);       
            cstatement.setString(1, mrpc);                                     tu trafia ID = 29
            cstatement.setObject(2, inputParameters);                          przekazanie parametrów
            cstatement.registerOutParameter(3, Types.VARCHAR, "RETVAL");     
            results = cstatement.executeQuery();
 
            if (results.next()) {
                        response = results.getString(1).concat("OK");          iteracja po wyniku
            }
 
                  
            }
        catch (Exception e) {
            System.out.print("\n Error:"+e.getMessage());
        }
 
}
</pre>

Dekompozycja url’a:

<table width="722">
  <tr>
    <td width="114">
      protocol
    </td>
    
    <td width="138">
      jdbc:sanchez
    </td>
    
    <td width="54">
      Y
    </td>
    
    <td width="90">
    </td>
    
    <td width="181">
      driver protocol
    </td>
  </tr>
  
  <tr>
    <td width="114">
      database
    </td>
    
    <td width="138">
      ipAddress:port:serverType
    </td>
    
    <td width="54">
      Y
    </td>
    
    <td width="90">
    </td>
    
    <td width="181">
      ipAddress, targeted server IP address;</p> 
      
      <p>
        port, targeted server port number;
      </p>
      
      <p>
        serverType, Sanchez Profile server type</td> </tr> 
        
        <tr>
          <td width="114">
            transType
          </td>
          
          <td width="138">
            MTM or MQ
          </td>
          
          <td width="54">
            N
          </td>
          
          <td width="90">
            MTM
          </td>
          
          <td width="181">
            Transaction type, which independent of message type. The basic transports supported currently are MTM, a socket-based transport and MQ Series transport.
          </td>
        </tr>
        
        <tr>
          <td width="114">
            locale
          </td>
          
          <td width="138">
            countryCode:languageCode
          </td>
          
          <td width="54">
            N
          </td>
          
          <td width="90">
            US:EN
          </td>
          
          <td width="181">
            Locale is Java’s standard means of identifying a language for the purposes of internationalization and localization.</p> 
            
            <p>
              CountryCode: a non-null string containing a two-letetr ISO-639 country code.
            </p>
            
            <p>
              LanguageCode: a non-null string containing a two-letetr ISO-639 language code.</td> </tr> 
              
              <tr>
                <td width="114">
                  dateFormat
                </td>
                
                <td width="138">
                  Pattern
                </td>
                
                <td width="54">
                  N
                </td>
                
                <td width="90">
                  Yyyy-mm-dd
                </td>
                
                <td width="181">
                  The pattern date string
                </td>
              </tr>
              
              <tr>
                <td width="114">
                  timeFormat
                </td>
                
                <td width="138">
                  Pattern
                </td>
                
                <td width="54">
                  N
                </td>
                
                <td width="90">
                  h:mm:ss
                </td>
                
                <td width="181">
                  The pattern time string
                </td>
              </tr>
              
              <tr>
                <td width="114">
                  rowPrefetch
                </td>
                
                <td width="138">
                  Row
                </td>
                
                <td width="54">
                  N
                </td>
                
                <td width="90">
                  30
                </td>
                
                <td width="181">
                  Give the JDBC driver a hint as to the number of rows that should be fetched from the database when more rows are needed for this result set.
                </td>
              </tr>
              
              <tr>
                <td width="114">
                  timeOut
                </td>
                
                <td width="138">
                  time/s
                </td>
                
                <td width="54">
                  N
                </td>
                
                <td width="90">
                  2
                </td>
                
                <td width="181">
                  Sets the number of seconds the driver will wait for a Statement to execute to the given number of seconds. If the limit is exceeded, a SQLException is thrown.
                </td>
              </tr>
              
              <tr>
                <td width="114">
                  signOnType
                </td>
                
                <td width="138">
                  0 or 1
                </td>
                
                <td width="54">
                  N
                </td>
                
                <td width="90">
                </td>
                
                <td width="181">
                  0, sign on at clear password; 1, sign on at challenge/response by encrypted password.
                </td>
              </tr>
              
              <tr>
                <td width="114">
                  numberFormat</p> 
                  
                  <p>
                    &nbsp;</td> 
                    
                    <td width="138">
                      Pattern
                    </td>
                    
                    <td width="54">
                      N
                    </td>
                    
                    <td width="90">
                      ####.##
                    </td>
                    
                    <td width="181">
                      The pattern number string
                    </td></tr> 
                    
                    <tr>
                      <td width="114">
                        poolSize
                      </td>
                      
                      <td width="138">
                      </td>
                      
                      <td width="54">
                        N
                      </td>
                      
                      <td width="90">
                        80
                      </td>
                      
                      <td width="181">
                        Connection pool size.
                      </td>
                    </tr></tbody> </table> 
                    
                    <p>
                      2. Komunikacja poprzez PBSSRV jest logowana w MSGLOG (ogólne informacje), detale w postaci komunikatów wejście/wyjście możesz uzyskać poprzez włączenie flag na poziomie konfiguracji typu SV servera CTBLSVTYP wtedy wpisy będą się pojawiac również w tablicy MSGLOGSEQ (i jedna i druga są na tym samym globalu ^MSGLOG).
                    </p>
                    
                    <p>
                      &nbsp;
                    </p>
                    
                    <p>
                      LOGMSG
                    </p>
                    
                    <p>
                      SVTYP  6 Log Incoming Messages                     1  L        N
                    </p>
                    
                    <p>
                      LOGREPLY
                    </p>
                    
                    <p>
                      SVTYP  7 Log Reply Messages                        1  L        N
                    </p>
                    
                    <p>
                      &nbsp;
                    </p>
                    
                    <p>
                      Tak jak wspominałem informacje przechowywane w tablicy MSGLOGSEQ są przechowywane w oryginalnej postaci w formacie LV. Narzędzia do transformacji tego formatu i opis w ogóle koncepcji jest z grubsza opisany w komentarzach w procedurze MSG (sekcje LV, V2LV, LV2V) w v7, z piątce w rutynie MSG.M też.
                    </p>