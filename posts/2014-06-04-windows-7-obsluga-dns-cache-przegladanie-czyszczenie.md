---
title: 'Windows 7: Obsługa DNS cache – przeglądanie, czyszczenie'
author: szydell
type: post
date: 2014-06-04T12:32:41+00:00
url: /post/2014/windows-7-obsluga-dns-cache-przegladanie-czyszczenie/
categories:
  - Windows
tags:
  - dns
  - dns cache
  - flush
  - windows
  - windows 7

---
Jak można sobie wyczyścić lokalny cache dns na komputerze z Windows 7?

<pre>ipconfig /flushdns
</pre>

Można też przed wyczyszczeniem wylistować zawartość cache:

<pre>ipconfig /displaydns
</pre>

No i na sam koniec całkowite wyłączenie cache w ramach obecnej sesji (do restartu):

<pre>net stop dnscache
</pre>

&nbsp;