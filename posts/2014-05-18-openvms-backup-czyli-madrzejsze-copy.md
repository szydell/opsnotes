---
title: 'OpenVMS: backup – czyli mądrzejsze copy'
author: szydell
type: post
date: 2014-05-18T20:22:46+00:00
url: /post/2014/openvms-backup-czyli-madrzejsze-copy/
categories:
  - OpenVMS
tags:
  - Alpha
  - backup
  - OpenVMS
  - tar

---
Pomijając oczywiste zastosowanie tej komendy do backupów taśmowych, zdarza mi się czasami użyć jej do kopiowania ważnych plików między różnymi lokalizacjami. Czasami też muszę zrobić backup zestawu plików do jednego tzw. 'save setu&#8217;. Po ludzku pisząc &#8211; tar.

  1. bezpieczne przekopiowywanie plików (czyli z weryfikacją): 
    <pre>backup disk$zrodlo:[katalog]*.*;* disk$cel:[katalog] /verify /log
</pre>

  2. jak wyżej tylko, że z podkatalogami: 
    <pre>backup disk$zrodlo:[katalog...]*.*;* disk$cel:[katalog...] /verify /log
</pre>

  3. Zrobienie backupu większej ilości plików i podkatalogów do jednego pliku (czyli takie ztarowanie): 
    <pre>backup disk$zrodlo:[katalog...]*.*;* disk$cel:[katalog]plik_z_backupem.bck /verify /log /save_set
</pre>

&nbsp;