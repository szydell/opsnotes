---
title: SELinux dla opornych
author: szydell
type: post
date: 2019-05-22T17:25:08+00:00
url: /post/2019/selinux-dla-opornych/
categories:
  - Fedora
  - Linux
tags:
  - auditd
  - sealert
  - SELinux

---
Czyli jak rozwiązać problem z serii 'coś nie działa&#8217;. 🙂

W środowiskach chronionych SELinuxem, często dochodzi się do sytuacji w której jest ok, ale 'coś nie działa&#8217;. Dostajemy jakieś permission denied, czy inny nic nam nie mówiący komunikat. Gdzie szukać podpowiedzi?

Na Fedorze trzeba podejrzeć

<pre class="wp-block-code"><code>/var/log/audit/audit.log</code></pre>

Jeżeli widzimy tam linie ze słówkiem 'deny&#8217;, to jest duże prawdopodobieństwo, że znaleźliśmy winowajcę. Co dalej?

Przepuszczamy nasz plik loga przez skrypt tłumaczący co zrobić.

<pre class="wp-block-code"><code>sealert -a audit.log > wynik.txt</code></pre>

W pliku wynik będziemy mieli łopatologicznie wytłumaczone co zrobić, żeby dane deny znikło. Czasami podpowiedzi może być kilka. Trzeba wybrać czy np. przełaczamy jakąś flagę globalnie, a może lepiej poprawić kontekst jednego pliku. Tutaj wybór należy już do nas.

Na koniec najważniejsze&#8230; SELinux jest skuteczny. Eliminuje sporo zagrożeń. Wyłączanie go to bardzo, ale to bardzo durny pomysł.