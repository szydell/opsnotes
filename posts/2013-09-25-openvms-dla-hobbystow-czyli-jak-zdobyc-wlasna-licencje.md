---
title: OpenVMS dla hobbystów, czyli jak zdobyć własną licencję
author: szydell
type: post
date: 2013-09-25T13:38:25+00:00
url: /post/2013/openvms-dla-hobbystow-czyli-jak-zdobyc-wlasna-licencje/
categories:
  - OpenVMS
tags:
  - Alpha
  - DECUServe
  - hobby
  - licencja
  - OpenVMS

---
Na początek musimy zostać członkami DECUServe. Automatyczne założenie konta możliwe jest przez telnet (telnet decuserve.org). Jako username podajemy REGISTRATION i wykonujemy kolejne kroki. Ważne jest, abyśmy zanotowali sobie nasz numer członkowski, który wygląda mniej więcej tak US000000.

Kolejny krok, to wypełnienie ankiety na stronie: <a href="http://www.openvms.org/pages.php?page=Hobbyist" target="_blank">http://www.openvms.org/pages.php?page=Hobbyist</a>  
Oczywiście musimy podać tam nasz numer DECUServe.

Teraz pozostaje oczekiwanie. Ja swojego PAK&#8217;a po 3 dniach od wysłania ankiety.