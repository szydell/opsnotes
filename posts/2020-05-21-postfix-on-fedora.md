---
title: postfix, brak public/pickup
author: szydell
type: post
date: 2020-05-21T18:30:00+00:00
url: /post/2020/postfix-on-fedora/
featured_image: /wp-content/uploads/2020/05/postfix.gif
categories:
  - Fedora
  - postfix
tags:
  - mkfifo
  - pickup

---
Do lokalnego dostarczania poczty miałem zainstalowane estmp. Chyba jest to rozwiązanie 'standardowe&#8217; w Fedorze. Dziwnie działało, potrafiło mi zjeść całego core&#8217;a, także beż żalu zmieniłem na lepiej mi znanego postfixa.

W logach odkryłem, że z postfixem też coś nie halo:

<pre class="wp-block-preformatted">laPtak postfix/postdrop[7937]: warning: unable to look up public/pickup: No such file or directory</pre>

Rozwiązanie:

<pre class="wp-block-preformatted">sudo <code>mkfifo /var/spool/postfix/public/pickup</code>
sudo systemctl restart postfix</pre>