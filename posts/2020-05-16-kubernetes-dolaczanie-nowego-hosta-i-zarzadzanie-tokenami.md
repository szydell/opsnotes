---
title: Kubernetes, dołączanie nowego hosta i zarzadzanie tokenami
author: szydell
type: post
date: 2020-05-16T14:20:03+00:00
url: /post/2020/kubernetes-dolaczanie-nowego-hosta-i-zarzadzanie-tokenami/
featured_image: /wp-content/uploads/2020/05/kubernetes-logo-604x270.jpg
categories:
  - DevOps
  - Kubernetes
tags:
  - join
  - kubeadm
  - token

---
Standardowo token potrzebny do dołączenia do klastra ważny jest 24 godziny. Poza tym, notatki giną. 😉

### Komenda pozwalająca na dołączenia hosta do klastra {.wp-block-heading}

<pre class="wp-block-preformatted"># <code>kubeadm join --token &lt;token&gt; &lt;control-plane-host&gt;:&lt;control-plane-port&gt; --discovery-token-ca-cert-hash sha256:&lt;hash&gt;</code></pre>

### Wylistowanie aktywnych tokenów {.wp-block-heading}

<pre class="wp-block-preformatted"><code>kubeadm token list</code></pre>

Pusto? Znaczy się, nie masz żadnego aktywnego tokena.

### Wygeneruj nowy token {.wp-block-heading}

<pre class="wp-block-preformatted"><code>kubeadm token create</code></pre>

### Odzyskaj 'discovery token ca cert&#8217; {.wp-block-heading}

Poza tokenem, do komendy kubeadm join potrzeba też hasha certyfikatu. Żeby go 'odzyskać&#8217;, wykonaj:

<pre class="wp-block-preformatted"><code>openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2&gt;/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //'</code></pre>