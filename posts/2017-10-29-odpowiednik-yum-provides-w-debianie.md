---
title: "Odpowiednik 'yum provides’ w Debianie"
author: szydell
type: post
date: 2017-10-29T10:15:31+00:00
url: /post/2017/odpowiednik-yum-provides-w-debianie/
categories:
  - Linux
tags:
  - apt
  - Debian
  - yum

---
Przez większość życia pracuję na linuxach redhatopodobnych. Czasami jednak trzeba popracować na debianie 🙂  
Co zamiast 'yum provides&#8217;?

Instalacja pakietu apt-file i skorzystanie z 'apt-file search&#8217;. Efekt niemal identyczny.