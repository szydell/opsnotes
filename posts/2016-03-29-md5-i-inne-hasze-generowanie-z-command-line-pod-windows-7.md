---
title: MD5 i inne hasze – generowanie z command line pod Windows 7
author: szydell
type: post
date: 2016-03-29T12:35:08+00:00
url: /post/2016/md5-i-inne-hasze-generowanie-z-command-line-pod-windows-7/
categories:
  - Aplikacje
  - Windows
tags:
  - CertUtil
  - hash
  - MD2
  - MD4
  - MD5
  - SHA1
  - SHA256
  - SHA384
  - SHA512

---
Windows 7 posiada wbudowane narzędzie, które można zmusić do wygenerowania MD5.  
Rozwiązanie nie na wprost, ale działa 🙂

certUtil -hashfile pathToFileToCheck [HashAlgorithm]

HashAlgorithm przyjmuje parametry: MD2 MD4 MD5 SHA1 SHA256 SHA384 SHA512

Przykładowo generowanie MD5 dla pliku C:\TEMP\MyDataFile.img:  
<?prettify linenums=true?>

<pre class="prettyprint">CertUtil -hashfile C:\TEMP\MyDataFile.img MD5</pre>

Original solution by Laisvis Lingvevicius