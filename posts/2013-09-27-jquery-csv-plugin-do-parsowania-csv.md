---
title: jQuery-CSV – plugin do parsowania CSV
author: szydell
type: post
date: 2013-09-27T12:29:49+00:00
url: /post/2013/jquery-csv-plugin-do-parsowania-csv/
categories:
  - JScript
tags:
  - .csv
  - jQuery
  - jQuery-CSV

---
Kolejny przydatny klocek, do budowania projektów łebowych.

Rozbudowuje jQuery o możliwość skutecznego parsowania plików w formacie .csv. W przeglądarkach zgodnych z html5 pozwala nawet zassać taki plik z lokalnego dysku 🙂

<a title="jQuery-CSV" href="http://code.google.com/p/jquery-csv/" target="_blank">http://code.google.com/p/jquery-csv/</a>

Wersja dostępna w momencie pisania tej notatki nie potrafi jeszcze zapisywać do .csv, ale to już tylko kwestia czasu.