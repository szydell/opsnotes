---
title: 'RHEL 7: zmiana hasła roota'
author: szydell
type: post
date: 2015-03-21T10:14:22+00:00
url: /post/2015/rhel-7-zmiana-hasla-roota/
categories:
  - Linux
tags:
  - CentOS
  - centos 7
  - grub
  - grub2
  - hasło
  - rd.break
  - Red Hat Enterprise Linux
  - Red Hat Enterprise Linux 7
  - RHEL
  - RHEL 7
  - root
  - systemd

---
Metoda działa na Red Hat Enterprise Linux 7 oraz Centos 7.

  * W trakcie startu systemu po wyświetleniu menu gruba zatrzymujemy go (strzałka w górę np.). Wybieramy z listy kernel, który chcemy użyć (zapewne będzie to pierwsza linia od góry).
  * Wciskamy 'e&#8217;
  * Znajdujemy linię, która zaczyna się od 'linux16&#8242;
  * Za słowem 'quiet&#8217; lub przed 'LANG&#8217; dodajemy wpis 'rd.break&#8217;
  * Wciskamy CTRL-X
  * Powinien wystartować goły system, z promptem #
  * `# mount -o remount,rw /sysroot`
  * `# chroot /sysroot`
  * `# passwd` &#8211; wpisujemy nowe hasło
  * `# touch /.autorelabel` &#8211; wymuszamy odtworzenie kontekstów SELinuxa przy starcie
  * exit, exit

Koniec.