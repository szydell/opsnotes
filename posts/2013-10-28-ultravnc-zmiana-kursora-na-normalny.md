---
title: UltraVNC – zmiana kursora na normalny
author: szydell
type: post
date: 2013-10-28T10:43:27+00:00
url: /post/2013/ultravnc-zmiana-kursora-na-normalny/
categories:
  - VNC
tags:
  - kursor
  - UltraVNC
  - VNC

---
Standardowo UltraVNC zamiast kursora używa mało widocznej kropki. Jak dla mnie niewidocznej i przez to wkurzającej. Żeby nie było zbyt prosto twórcy aplikacji nie przewidzieli konfiguracji wyglądu kursora w ustawieniach konfiguracyjnych, ba uruchomienie viewera z opcją /? nie pokazuje niczego co mogłoby się kojarzyć z kursorem&#8230; a jednak to tam leży rozwiązanie.

Viewera należy uruchamiać z opcją **/normalcursor**.

Taka 'ukryta funkcja&#8217; 😉

W ramach kosmetyki można też w samej konfiguracji zaznaczyć sobie opcję &#8222;Don&#8217;t show remote cursor&#8221;.

[<img loading="lazy" decoding="async" class="size-medium wp-image-81 aligncenter" alt="Nie pokazuj zdalnego kursora" src="https://marcin.szydelscy.pl/wp-content/uploads/2013/10/uvnc_kursor-300x238.jpg" width="300" height="238" />][1]

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2013/10/uvnc_kursor.jpg