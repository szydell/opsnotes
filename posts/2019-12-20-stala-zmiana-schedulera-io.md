---
title: Stała zmiana schedulera IO
author: szydell
type: post
date: 2019-12-20T17:11:24+00:00
url: /post/2019/stala-zmiana-schedulera-io/
categories:
  - Linux
tags:
  - grub2
  - grubby
  - io
  - noop
  - scheduler
  - uefi

---
Hypervisory takie jak hyper-v, czy vmware optymalizują dostęp do dysku. Standardowy scheduler linuxa też. Może to obniżać ogólną wydajność.

W <a rel="noreferrer noopener" aria-label="sieci (opens in a new tab)" href="https://docs.microsoft.com/pl-pl/windows-server/virtualization/hyper-v/best-practices-for-running-linux-on-hyper-v" target="_blank">sieci</a> można znaleźć poradę, że problem optymalizowania dostępu do danych należy rozwiazać np. przez dodanie parametru do /etc/grub.conf. Oczywiście niewiele to pomaga, bo np. CentOs 7 takiego pliku w ogóle nie ma. Jest jednak w miarę uniwersalne rozwiązanie dla linuxów korzystających z gruba i UEFI:

<pre class="wp-block-code"><code>grubby --update-kernel=ALL --args="elevator=noop"</code></pre>

Niestety zmiana jest widoczna tylko w obecnie skonfigurowanych kernelach. Żeby była bardziej przyszłościowa, należy w pliku &#8222;/etc/default/grub&#8221; do linii GRUB\_CMDLINE\_LINUX= dopisać elevator=noop.