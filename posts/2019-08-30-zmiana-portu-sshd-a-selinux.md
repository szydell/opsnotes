---
title: zmiana portu sshd, a selinux i firewalld
author: szydell
type: post
date: 2019-08-30T18:29:29+00:00
url: /post/2019/zmiana-portu-sshd-a-selinux/
categories:
  - Linux
tags:
  - firewalld
  - SELinux
  - ssh

---
Standardowy port dla demona ssh to 22. Jeżeli chcemy ustawić coś niestandardowego, selinux nam na to nie pozwoli. Jak rozwiązać problem?

<pre class="wp-block-code"><code>(opcjonalnie) dnf install policycoreutils-python-utils
semanage port -a -t ssh_port_t -p tcp 1234
</code></pre>

Kolejny krok, to otworzenie niestandardowego portu na firewallu

<pre class="wp-block-code"><code>firewall-cmd --add-port=1234/tcp --permanent
firewall-cmd --reload</code></pre>