---
title: 'Wasteland 2: Director’s Cut'
author: szydell
type: post
date: 2019-12-13T11:43:53+00:00
url: /post/2019/wasteland-2-directors-cut/
ao_post_optimize:
  - 'a:6:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";s:15:"ao_post_preload";s:0:"";}'
categories:
  - GOG
  - Gry
  - Linux
tags:
  - lutris
  - wasteland 2

---
GOG ostatnio rozdawał za darmo, niestety na linuxie gra się crashuje.  
Rozwiązanie na szczęście dość proste. W pliku /etc/security/limits.conf trzeba dodać:

<pre class="wp-block-code"><code>&lt;twoj_login&gt; soft nofile 4096
&lt;twoj_login&gt; hard nofile 8192</code></pre>

Restart i powinno działać.

Link do profilu gry na Lutrisie: <https://lutris.net/games/wasteland-2-directors-cut/>