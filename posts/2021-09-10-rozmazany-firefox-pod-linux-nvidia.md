---
title: Rozmazany firefox pod Linux/nvidia
author: szydell
type: post
date: 2021-09-10T15:15:00+00:00
url: /post/2021/rozmazany-firefox-pod-linux-nvidia/
featured_image: /wp-content/uploads/2020/06/firefox-logo.png
ao_post_optimize:
  - 'a:5:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";}'
categories:
  - Aplikacje
tags:
  - firefox
  - nvidia

---
Efekt wyjątkowo paskudny i męczący oczy. Migoczący i rozmazany firefox na szczęście jest prosty do 'wyostrzenia&#8217;.

<ol class="is-style-default">
  <li>
    Uruchom <a href="https://download.nvidia.com/XFree86/Linux-x86_64/396.51/README/nvidiasettings.html" target="_blank" rel="noreferrer noopener">nvidia-settings</a>
  </li>
  <li>
    Idź do zakładki 'Antialiasing settings&#8217;
  </li>
  <li>
    Odznacz 'Enable FXAA&#8217;
  </li>
  <li>
    Nie zaznaczaj więcej 😛
  </li>
</ol>

No i zrestartuj firefoxa, bo zmiana działa tylko dla świeżo włączonych apek.