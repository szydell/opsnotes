---
title: SecureVNC Plugin – szyfrowanie sesji UltraVNC
author: szydell
type: post
date: 2013-09-26T09:15:43+00:00
url: /post/2013/securevnc-plugin-szyfrowanie-sesji-ultravnc/
categories:
  - VNC
tags:
  - SecureVNC
  - UltraVNC
  - VNC

---
Adam D. Walling stworzył genialnie prosty w użyciu plugin, pozwalający zaszyfrować silnym kluczem nasze połączenie VNC realizowane za pomocą UltraVNC.

Plugin w podstawowej (i zalecanej) wersji działa bezobsługowo. Cała praca polega na jego dograniu do katalogu OpenVNC i znaczeniu w konfiguracji, że chcemy w niego korzystać (zarówno na serwerze, jak i stacji klienckiej). Nie generujemy kluczy! Plugin sam zadba o szyfrowanie.

[<img loading="lazy" decoding="async" class="aligncenter size-medium wp-image-9" alt="openvmc securevnc plugin" src="https://marcin.szydelscy.pl/wp-content/uploads/2013/09/openvmc_securevnc_plugin_viewer-279x300.jpg" width="279" height="300" />][1]Standardowo plugin pracuje z wykorzystaniem kluczy 2048-bit RSA i 256-bit AES.

Oczywiście można zmienić zarówno używane sposoby szyfrowania, jak i wielkość kluczy, jednak nie jest to konieczne, ani zalecane.

Plugin można pobrać bezpośrednio ze strony: <a title="Adam D. Walling - SecureVNC plugin" href="http://adamwalling.com/SecureVNC/" target="_blank">http://adamwalling.com/SecureVNC/</a>

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2013/09/openvmc_securevnc_plugin_viewer.jpg