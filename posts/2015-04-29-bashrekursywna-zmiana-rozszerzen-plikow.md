---
title: '[bash]Rekursywna zmiana rozszerzeń plików.'
author: szydell
type: post
date: 2015-04-29T18:32:48+00:00
url: /post/2015/bashrekursywna-zmiana-rozszerzen-plikow/
categories:
  - bash
tags:
  - awk
  - bash
  - find
  - xargs

---
Załóżmy, że mamy dużo plików o jakimś tam rozszerzeniu rozsianych w dużej ilości katalogów.  
I chcemy tym wszystkim plikom zmienić rozszerzenie na jakieś inne.  
np  
`test/bb/plik2.jpeg<br />
test/cc/plik4.jpeg<br />
test/cc/plik3.jpeg<br />
test/cc/plik5.jpeg<br />
test/aa/plik.jpeg<br />
test/aa/dd/plik7.jpeg`

chcemy te pliki. jpeg zmienić na .jpg

Rozwiązanie:  
`find test -name *.jpeg | awk -F '.' '{OFS=FS;var=$0;$(NF)="";print var" "$0"jpg"}' | xargs -n 2 mv`