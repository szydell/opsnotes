---
title: Konfiguracja SSL/TLS w różnych serwerach http.
author: szydell
type: post
date: 2020-06-08T07:47:00+00:00
url: /post/2020/konfiguracja-ssl-tls-w-roznych-serwerach-http/
categories:
  - apache httpd
  - nginx
tags:
  - https
  - OpenSSL
  - ssl
  - tls

---
Kto pamięta bezpieczny zestaw ssl_cipher niech pierwszy rzuci kamieniem. Ja nie.

<a href="https://ssl-config.mozilla.org/" target="_blank" rel="noreferrer noopener">https://ssl-config.mozilla.org/</a>

Proste i skuteczne narzędzie, w którym wybieramy serwer, wersję jego i OpenSSL, poziom paranoi&#8230; kopiuj-wklej i do boju. Gotowe. 🙂