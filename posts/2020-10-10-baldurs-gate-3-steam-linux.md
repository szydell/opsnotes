---
title: Baldur’s Gate 3 Steam/Linux, problemy z instalacją
author: szydell
type: post
date: 2020-10-10T20:13:30+00:00
url: /post/2020/baldurs-gate-3-steam-linux/
featured_image: /wp-content/uploads/2020/10/Screenshot_20201010_220645-452x270.png
categories:
  - Aplikacje
  - Gry
tags:
  - 'Baldur&#039;s Gate'
  - Proton
  - protontricks
  - steam

---
Zapewne szybko to poprawią, ale kiedy to piszę <a href="https://www.protondb.com/app/1086940" target="_blank" rel="noreferrer noopener">Baldur&#8217;s Gate 3</a> Steam nie działa po instalacji. Rozwiązanie okazuje się być dość proste:

<pre class="wp-block-preformatted">sudo dnf install python3-pip python3-setuptools python3-libs pipx
pipx install protontricks
protontricks 1086940 dotnet48</pre>

O co chodzi? Protontricks to Winetricks skonfigurowane do pracy ze Steamem.  
1086940 to id gry Baldur&#8217;s Gate  
A dotnet48, to brakujący kawałek softu przez który gra nie próbuje nawet wystartować.