---
title: firewalld – przekierowywanie portów
author: szydell
type: post
date: 2021-07-20T15:55:23+00:00
url: /post/2021/firewalld-przekierowywanie-portow/
featured_image: /wp-content/uploads/2021/07/firewalld.org_.png
categories:
  - Fedora
  - Linux
tags:
  - firewall-cmd
  - firewalld
  - port forward
  - przekierowanie portu

---
Lubię firewalld. Nie zawsze jednak pamiętam składnię <a href="https://firewalld.org/documentation/utilities/firewall-cmd.html" target="_blank" rel="noreferrer noopener">firewall-cmd</a>.  
Szybka notatka jak zestawić przekierowanie portu na inne ip i port.

Uruchom jako root:

<pre class="wp-block-preformatted"># firewall-cmd --permanent --zone=public --add-forward-port=port=80:proto=tcp:toport=8080:toaddr=10.1.1.10</pre>

Rezultatem tej komendy będzie ustanowienie przekierowania z portu **80**(port) twojej maszyny, na port **8080**(toport) maszyny o adresie **10.1.1.10**(toaddr). Jeżeli chcesz przekierować porty wewnątrz jednego hosta, pomiń część 'toaddr&#8217;.

Co dalej? Standardowo:

<pre class="wp-block-preformatted"># firewall-cmd --reload
# firewall-cmd --list-all</pre>

Pamiętaj, że samo przekierowanie portu może nie wystarczyć. Musisz go mieć otwartego. W tym celu wykonaj dodatkowo komendę:

<pre class="wp-block-preformatted"># firewall-cmd --add-port=80/tcp --permanent
# firewall-cmd --reload</pre>

I zweryfikuj czy pojawia się na liście otwartych portów:

<pre class="wp-block-preformatted">#firewall-cmd --list-all</pre>

Przekierowanie oczywiście można też usunąć.  
Wystarczy zamienić w komendzie dodawania słówko add na remove:

<pre class="wp-block-preformatted"># firewall-cmd --permanent --zone=public --remove-forward-port=port=80:proto=tcp:toport=8080:toaddr=10.1.1.10
</pre>

Firewalld powinien ogłosić sukces, a nam pozostaje tylko załadować zmiany do pamięci komendą firewall-cmd &#8211;reload.