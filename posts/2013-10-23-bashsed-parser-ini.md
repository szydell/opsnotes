---
title: bash/sed parser .ini
author: szydell
type: post
date: 2013-10-23T15:35:30+00:00
url: /post/2013/bashsed-parser-ini/
categories:
  - bash
tags:
  - .ini
  - bash
  - sed

---
Potrzebowałem dzisiaj prosty parser, dla pliku konfiguracyjnego w moim skrypcie pisanym w bashu. Padło na .ini i genialne wygooglowane rozwiązanie

&nbsp;

<pre>#!/bin/bash
CONFIG_FILE="config.ini"
SECTION="section_1"

eval `sed -e 's/[[:space:]]*\=[[:space:]]*/=/g' \
          -e 's/;.*$//' \ -e 's/[[:space:]]*$//' \
          -e 's/^[[:space:]]*//' \
          -e "s/^\(.*\)=\([^\"']*\)$/\1=\"\2\"/" \
          &lt; $CONFIG_FILE \
          | sed -n -e "/^\[$SECTION\]/,/^\s*\[/{/^[^;].*\=.*/p;}"`</pre>

Co ważne autor pomysłu udostępnia go zgodnie z zasadami licencji WTFPL. 😉

Oryginał dostępny tu: <a title=".ini parser" href="http://www.tuxz.net/blog/archives/2011/10/19/parse__ini_files_with_bash_and_sed/" target="_blank">http://www.tuxz.net/blog/archives/2011/10/19/parse__ini_files_with_bash_and_sed/</a>