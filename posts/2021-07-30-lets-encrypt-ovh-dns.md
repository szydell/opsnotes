---
title: SSL/TLS z Let’s Encrypt dla domen w OVH.
author: szydell
type: post
date: 2021-07-30T19:41:54+00:00
url: /post/2021/lets-encrypt-ovh-dns/
featured_image: /wp-content/uploads/2021/07/letsencrypt_i_ovh.png
ao_post_optimize:
  - 'a:5:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";}'
categories:
  - DevOps
  - Docker
  - Linux
tags:
  - certbot
  - certyfikaty
  - dns
  - dns challenge
  - dns mode
  - docker
  - domena
  - 'let&#039;s encrypt'
  - ovh
  - podman
  - ssl
  - systemd
  - tls
  - wildcard

---
Let&#8217;s Encrypt ma plugin dla OVH, skorzystamy więc z niego w poniższym tutorialu. Opisuje w nim krok po kroku jak skonfigurować automatyczne generowanie i odświeżanie certyfikatów z wykorzystaniem <a href="https://letsencrypt.org/docs/challenge-types/#dns-01-challenge" target="_blank" rel="noreferrer noopener">dns-01 challenge</a>. 

Instrukcja ta ma zastosowanie tylko gdy Twoja domena hostowana jest przez OVH, wykorzystywać bowiem będziemy API tej firmy.

**Pokazane rozwiązanie pozwala na generowanie certyfikatów dla pojedynczych domen, oraz wildcardów.** 

Żeby było jeszcze ciekawiej, skorzystamy z oficjalnego obrazu <a href="https://hub.docker.com/r/certbot/dns-ovh" target="_blank" rel="noreferrer noopener">certbot/dns-ovh</a>, także tutorial ten można zastosować praktycznie na każdym linuxie z dockerem lub podmanem. No i przy okazji jest szansa, że będzie przez jakiś czas aktualny. 😉

## 1. Generowanie credentiali w OVH {.wp-block-heading}

Na stronie <a href="https://api.ovh.com/createToken/" target="_blank" rel="noreferrer noopener">https://api.ovh.com/createToken/</a> musimy wypełnić formularz w celu wygenerowania danych używanych później do tworzenia i kasowania wpisów dns. Zgodnie z <a href="https://certbot-dns-ovh.readthedocs.io/en/stable/" target="_blank" rel="noreferrer noopener">dokumentacją pluginu certbot-dns-ovh</a>, by móc potwierdzić bycie właścicielem domeny na potrzeby uzyskania certyfikatu Let&#8217;s Encrypt, będziemy potrzebować dostępu do następujących metod udostępnionych przez API OVH:

  * GET /domain/zone/*
  * PUT /domain/zone/*
  * POST /domain/zone/*
  * DELETE /domain/zone/*

Tak wygląda formularz, który wypełniłem dla swojego rozwiązania:<figure class="wp-block-image size-full">

<img loading="lazy" decoding="async" width="543" height="607" src="https://marcin.szydelscy.pl/wp-content/uploads/2021/07/image.png" alt="" class="wp-image-894" srcset="https://marcin.szydelscy.pl/wp-content/uploads/2021/07/image.png 543w, https://marcin.szydelscy.pl/wp-content/uploads/2021/07/image.png 268w" sizes="(max-width: 543px) 100vw, 543px" /> <figcaption>OVH API, Creating API Keys for your script</figcaption></figure> 

Klikamy na 'Create keys&#8217;, jeżeli macie skonfigurowane 2FA OVH poprosi o zatwierdzenie akcji, a rezulatem będzie tabelka zawierająca:

  1. Script name
  2. Script Description
  3. Application Key
  4. Application Secret
  5. Consumer Key

Notujemy z boku te wartości, będą za chwilkę potrzebne.

## 2. Konfiguracja certbota {.wp-block-heading}

Kolejny krok polega na stworzeniu pliku zawierającego dane pozwalające na autentykację w api OVH. Muszą się w nim znaleźć cztery kluczowe informacje &#8211; dns\_ovh\_endpoint, dns\_ovh\_application\_key, dns\_ovh\_application\_secret, dns\_ovh\_consumer_key. 

Ja założyłem ten plik w /etc/certbot/ovh-secrets.conf. Pamiętajcie, że raczej nie chcecie, żeby ktoś mógł tworzyć, czy kasować subdomeny w waszym imieniu, także zabezpieczcie ten pliczek. Powiedzmy, że wystarczy chmod 0600 /etc/certbot/ovh-secrets.conf.

Przykładowy plik konfiguracyjny:

<pre class="wp-block-code"><code>dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = 7rPawafweAJYWNvhH
dns_ovh_application_secret = 4SFtGD012npocan90nc0qkTWBflirSEf8oo
dns_ovh_consumer_key = Et3lLNe2fthx3MeMFaHe3ssoS71qQZHZ
</code></pre>

<pre class="wp-block-verse">Jeżeli przygotowujesz konfigurację dla infrastruktury zarządzanej przez OVH w Ameryce Północnej, parametr dns_ovh_endpoint powinien mieć wartość ovh-ca. </pre>

## 3. Katalogi, uprawnienia, selinux {.wp-block-heading}

Przygotuj potrzebne do działania katalogi i ustaw odpowiednie uprawnienia. W tym tutorialu przyjmuję, że certbot będzie uruchamiany jako root i chcemy trzymać certyfikaty w głównej strukturze systemu (/etc, /var).

<pre class="wp-block-code"><code>mkdir /etc/letsencrypt /var/lib/letsencrypt /var/log/letsencrypt
chmod 0700 /etc/letsencrypt /var/lib/letsencrypt /var/log/letsencrypt
</code></pre>

### 3.1 SELinux? {.wp-block-heading}

Na Fedorze SELinux zablokował mi certbota przy próbie zapisu do powyższych katalogów. Jako, że będzie do nich pisać tylko ta aplikacja, ustawiłem/dodałem im kontekst container\_file\_t.

<pre class="wp-block-code"><code># semanage fcontext -a -t container_file_t '/etc/letsencrypt'
# restorecon -v /etc/letsencrypt/
# semanage fcontext -a -t container_var_lib_t /var/lib/letsencrypt
# restorecon -v /var/lib/letsencrypt
# semanage fcontext -a -t container_file_t /var/log/letsencrypt
# restorecon -v /var/log/letsencrypt
</code></pre>

### 3.2 Logrotate {.wp-block-heading}

Jak można się domyślić po tym, że zakładamy dedykowany katalog w /var/log, będziemy zbierać logi. Logi zbyt długo trzymane to zło, także dodaj sobie do katalogu /etc/logrotate.d pliczek certbot, którego zawartość to:

<div class="wp-block-coblocks-gist">
  <noscript>
    <a href="https://gist.github.com/szydell/bc2a7ef7db269b780a078ee5556fb436">Zobacz ten gist w serwisie GitHub</a>
  </noscript>
</div>

  
Dzięki temu będziemy trzymać logi przez 60 dni, no i log będzie 'łamany&#8217; codziennie. Co oczywiste chcemy też starsze logi pakować, a użyjemy do tego komendy xz.

## 4. Pierwsze uruchomienie {.wp-block-heading}

<div class="wp-block-coblocks-alert" aria-label="Alert section">
  <p class="wp-block-coblocks-alert__title">
    Podman czy Docker?
  </p>
  
  <p class="wp-block-coblocks-alert__text">
    To zależy&#8230; Ja pracuję na Fedorze, lubię podmana i w tutorialu będę używał komendy podman. <br /><br /><strong>Co gdy używasz Dockera?</strong><br />Składnia jest identyczna, także zamień słówko podman na docker i wszystko będzie działać bez problemu. 🙂
  </p>
</div>

<pre class="wp-block-code"><code>podman run \
 --rm \
 --name letsencrypt \
 -v /etc/certbot/ovh-secrets.conf:/ovh-secrets.conf \
 -v /etc/letsencrypt:/etc/letsencrypt \
 -v /var/lib/letsencrypt:/var/lib/letsencrypt \
 -v /var/log/letsencrypt:/var/log/letsencrypt \ 
 certbot/dns-ovh:latest certonly \
   --non-interactive \
   --agree-tos -m twój@email.pl \ 
   --dns-ovh
   --dns-ovh-credentials /ovh-secrets.conf \
   --dns-ovh-propagation-seconds 10 \
   -d example.com -d *.example.com -d innadomena.pl</code></pre>

Ło panie, a dłuższej tej komendy się nie dało? Przeanalizujmy linia po linii co tu się dzieje:

  1. podman run &#8211; uruchom kontener (możesz zmienić na docker run, jeżeli używasz dockera)
  2. &#8211;rm &#8211; posprzątaj po zakończeniu życia kontenera
  3. &#8211;name letsencrypt &#8211; kontener ma się nazywać letsencrypt
  4. -v ścieżka\_na\_serwerze:ścieżka\_w\_kontenerze &#8211; te pliki i katalogi z serwera będą dostępne w kontenerze. Tam gdzie sobie zażyczyliśmy.
  5. certbot/dns-ovh:latest certonly &#8211; uruchamiamy najnowszą wersję kontenera dns-ovh. To w nim zainstalowany jest certbot, wraz z naszym pluginem do ovh. Instruujemy certbota, że ma działać w trybie 'certonly&#8217;.
  6. &#8211;non-interactive &#8211; nie chcemy być zbyt rozmowni i odpowiadać na pytania
  7. &#8211;agree-tos -m twój@email.pl &#8211; zgadzamy się na licencję i podajemy swojego maila, żeby Let&#8217;s Encrypt wiedziało z kim gadać na temat domen, którym wystawi certyfikat. W praktyce mail jest używany do powiadomienia, że certyfikat niedługo wygaśnie.
  8. &#8211;dns-ovh &#8211; odpal challenge dns01 z wykorzystaniem pluginu dns-ovh
  9. &#8211;dns-ovh-credentials /ovh-secrets.conf &#8211; plugin musi wiedzieć jak się zalogować do OVH, także pokazujemy mu gdzie leży przygotowany przez nas config.
 10. &#8211;dns-ovh-propagation-seconds 10 &#8211; halt! Dajmy ovh 10 sekund, żeby się zdążyła dodać domena.
 11. -d, -d, -d, -d itd. &#8211; Lista domen dla których chcemy dostać certyfikaty.

Przykładowy log:

<pre class="wp-block-code"><code># podman run --rm --name letsencrypt -v /etc/certbot/ovh-secrets.conf:/ovh-secrets.conf -v /etc/letsencrypt:/etc/letsencrypt -v /var/lib/letsencrypt:/var/lib/letsencrypt certbot/dns-ovh:latest certonly --agree-tos -m mój@email.pl  --dns-ovh --dns-ovh-credentials /ovh-secrets.conf --dns-ovh-propagation-seconds 10 -d example.com -d *.example.com

Requesting a certificate for example.com and example.com
Waiting 10 seconds for DNS changes to propagate

Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/example.com/fullchain.pem
Key is saved at:         /etc/letsencrypt/live/example.com/privkey.pem
This certificate expires on 2021-10-28.
These files will be updated when the certificate renews.

NEXT STEPS:
- The certificate will need to be renewed before it expires. Certbot can automatically renew the certificate in the background, but you may need to take steps to enable that functionality. See https://certbot.org/renewal-setup for instructions.
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
If you like Certbot, please consider supporting our work by:
 * Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
 * Donating to EFF:                    https://eff.org/donate-le
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</code></pre>

## 5. Mamy certyfikat, co dalej? {.wp-block-heading}

Zapewne po coś ten certyfikat był ci potrzebny. Apache httpd? Nginx? Haproxy? W tym tutorialu nic o nich nie będzie, ale tak. To są programy w których możesz użyć uzyskane właśnie certy.

### 5.1 Odnawianie {.wp-block-heading}

Po przeczytaniu loga na 100% (jak nie na 100000!) rzuciła ci się w oczy informacja, że certyfikaty trzeba odnawiać, i są raczej dość krótko ważne. Co z tym zrobić?

Stwórz sobie pliczek /usr/local/bin/certbot\_renew.sh (albo o innej nazwie!) i nadaj mu prawa do wykonywania (chmod +x /usr/local/bin/certbot\_renew.sh).  
Do środka wrzuć wywołanie certbota w trybie 'renew&#8217;, oczywiście w stylu dns-ovh. Żeby mieć pewność, że korzystam z najnowszej wersji kontenera dns-ovh, dorzuciłem jeszcze podman pull na początku:

<div class="wp-block-coblocks-gist">
  <noscript>
    <a href="https://gist.github.com/szydell/614fe970595942ee2f9e9c6a90b58b44">Zobacz ten gist w serwisie GitHub</a>
  </noscript>
</div>

  
Odpalamy testowo:

<pre class="wp-block-code"><code># vi /usr/local/bin/certbot_renew.sh
# chmod +x /usr/local/bin/certbot_renew.sh
# /usr/local/bin/certbot_renew.sh
Trying to pull docker.io/certbot/dns-ovh:latest...
Getting image source signatures
...
Copying blob 0b99e4c5dcd1 &#91;--------------------------------------] 0.0b / 0.0b
Copying config be6f9c73b8 done  
Writing manifest to image destination
Storing signatures
be6f9c73b8127bdd5983728deba3644efffb6826ff4d9be6757b2cf60c4aef5f

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Processing /etc/letsencrypt/renewal/example.com.conf
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Certificate not yet due for renewal

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
The following certificates are not due for renewal yet:
  /etc/letsencrypt/live/example.com/fullchain.pem expires on 2021-10-28 (skipped)
No renewals were attempted.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Saving debug log to /var/log/letsencrypt/letsencrypt.log
</code></pre>

### 5.2 systemd service i timer {.wp-block-heading}

Mamy już działający skrypcik, który gdy będzie czas odnowi certyfikaty. Wypadałoby teraz go uruchamiać cyklicznie. Cron jest trochę passe, także treningowo zróbmy to przy użyciu systemd.

Stwórz plik /etc/systemd/system/certbot_renew.service:

<div class="wp-block-coblocks-gist">
  <noscript>
    <a href="https://gist.github.com/szydell/cc9f9a53025864196287441348dea39f">View this gist on GitHub</a>
  </noscript>
</div>

  
Następnie timer, który będzie go uruchamiać (/etc/systemd/system/certbot_renew.timer):

<div class="wp-block-coblocks-gist">
  <noscript>
    <a href="https://gist.github.com/szydell/21495c0f94d3b73bc1497e9904fb99ac">View this gist on GitHub</a>
  </noscript>
</div>

  
Pozostaje nam już tylko przeładować ustawienia i włączyć timer:

<pre class="wp-block-code"><code># systemctl daemon-reload
# systemctl enable certbot_renew.timer 
Created symlink /etc/systemd/system/timers.target.wants/certbot_renew.timer → /etc/systemd/system/certbot_renew.timer.
</code></pre>

## Podsumowanie i aftercare Let&#8217;s Encrypt + OVH {.wp-block-heading}

Raz zdefiniowany proces odświeżania certyfikatów powinien być bezobsługowy, pamiętajmy jednak, że dobrze jest czasem zajrzeć do logów: 

  * journalctl -u certbot_renew.service
  * cat /var/log/letsencrypt/letsencrypt.log

No i na szczęście Let&#8217;s encrypt jest o tyle miłe, że w razie czego ostrzeże mailowo, gdy odnowienie certa nie wykona się o czasie.