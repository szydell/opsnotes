---
title: prof Chris Cieszewski – Piijiii, bziuuu! – mp3
author: szydell
type: post
date: 2013-10-24T05:01:17+00:00
url: /post/2013/prof-chris-cieszewski-piijiii-bziuuu-mp3/
enclosure:
  - |
    |
        https://marcin.szydelscy.pl/wp-content/uploads/2017/05/bziu.mp3
        24198
        audio/mpeg
        
categories:
  - Odloty
tags:
  - .mp3
  - ciszewski
  - dzwonek
  - sms

---
W ramach odlotów idealny dzwonek dla sms&#8217;ów.

<!--[if lt IE 9]><![endif]--><audio class="wp-audio-shortcode" id="audio-69-1" preload="none" style="width: 100%;" controls="controls"><source type="audio/mpeg" src="https://marcin.szydelscy.pl/wp-content/uploads/2017/05/bziu.mp3?_=1" />

<https://marcin.szydelscy.pl/wp-content/uploads/2017/05/bziu.mp3></audio>

Download: <a href="https://marcin.szydelscy.pl/wp-content/uploads/2017/05/bziu.mp3" target="_blank" rel="noopener noreferrer">bziuu.mp3</a>