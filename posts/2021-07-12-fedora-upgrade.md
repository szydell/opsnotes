---
title: Jak zaktualizować Fedorę?
author: szydell
type: post
date: 2021-07-12T20:43:09+00:00
url: /post/2021/fedora-upgrade/
featured_image: /wp-content/uploads/2021/07/Fedora_logo.svg
ao_post_optimize:
  - 'a:5:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";}'
categories:
  - Fedora
tags:
  - dnf
  - update
  - upgrade

---
Kolejnych kilka komend, których notorycznie zapominam. Jak zaktualizować Linux Fedora?

Chyba najprościej z terminala. Co prawda jest też jakiś [kreator graficzny][1], ale nie jestem fanem.

<pre class="wp-block-preformatted"># dnf upgrade --refresh
# <code>dnf install dnf-plugin-system-upgrade</code>
# <code>dnf system-upgrade download --releasever=</code>XX*
# <code>dnf system-upgrade reboot</code></pre>

  * W miejsce XX wpisujemy numerek wersji. Supportowany jest upgrade o 1 lub 2 oczka w górę. Szczerze mówiąc na skok o dwa się jeszcze nie odważyłem, ale o 1 idzie bez problemów.

 [1]: https://docs.fedoraproject.org/en-US/quick-docs/upgrading/#sect-upgrading-to-the-next-fedora-workstation-release