---
title: Animowane .ico? A co!
author: szydell
type: post
date: 2013-09-26T16:08:34+00:00
url: /post/2013/animowane-ico-a-co/
categories:
  - JScript
tags:
  - .ico
  - favico
  - favico.js

---
FB ma swoje zalety. Znajomy podrzucił mi niedawno dobrego tipa 🙂

<a title="favico.js" href="http://lab.ejci.net/favico.js/" target="_blank">http://lab.ejci.net/favico.js/</a>

świetne, proste, ładne, kolorowe, rusza się i w ogóle.

Favico to łatwa w obsłudze i wyjątkowo fajna biblioteka w java scripcie, gotowa do wykorzystania projektach 🙂

[<img loading="lazy" decoding="async" class="aligncenter size-full wp-image-50" alt="ico" src="https://marcin.szydelscy.pl/wp-content/uploads/2013/09/ico.png" width="140" height="140" />][1]

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2013/09/ico.png