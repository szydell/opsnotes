---
title: Go / Golang – instalacja na Centos 7
author: szydell
type: post
date: 2018-02-12T15:56:14+00:00
url: /post/2018/go-golang-instalacja-na-centos-7/
categories:
  - Go
tags:
  - CentOS
  - Go
  - Golang
  - repozytorium

---
Red Hat Enterprise Edition, a co za tym idzie również Centos są znane z dość konserwatywnego podejścia do wersji dostępnych pakietów.  
Kiedy piszę tę notatkę, w oficjalnym repozytorium CentOS dostępny Go dostępne jest w wersji 1.8.3, natomiast najnowszy stabilny release Go to wersja 1.10.  
Oczywiście instalacja ze źródeł, czy też z tar.gz z binarkami to żaden problem, ale nie po to wymyślono pakiety, żeby wszystko robić na około. Są na szczęście 'dobrzy ludzie&#8217;, którzy pomyśleli o zrobieniu repozytorium z aktualnymi paczkami, można je znaleźć pod adresem: <https://go-repo.io/>

Ściągawka do instalacji repo:

(root)  
<?prettify linenums=true?>

<pre class="prettyprint">rpm --import https://mirror.go-repo.io/centos/RPM-GPG-KEY-GO-REPO
curl -s https://mirror.go-repo.io/centos/go-repo.repo | tee /etc/yum.repos.d/go-repo.repo
yum install golang</pre>

Ściągawka do konfiguracji środowiska:  
(user do kodowania)  
<?prettify linenums=true?>

<pre class="prettyprint">mkdir -p ~/go/{bin,pkg,src}
echo 'export GOPATH="$HOME/go"' &gt;&gt; ~/.bashrc
echo 'export PATH="$PATH:${GOPATH//://bin:}/bin"' &gt;&gt; ~/.bashrc</pre>