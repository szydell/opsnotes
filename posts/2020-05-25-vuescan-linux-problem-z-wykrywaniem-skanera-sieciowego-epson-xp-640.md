---
title: VueScan Linux, problem z wykrywaniem skanera sieciowego Epson XP-640
author: szydell
type: post
date: 2020-05-25T14:41:00+00:00
url: /post/2020/vuescan-linux-problem-z-wykrywaniem-skanera-sieciowego-epson-xp-640/
featured_image: /wp-content/uploads/2020/05/vuescan.png
categories:
  - Aplikacje
  - Fedora
tags:
  - avahi-browse
  - scanner
  - skaner
  - tcpdump
  - VueScan

---
Problem niestety jest szerszy, bo dotyczy sposobu w jaki działa protokół wykrywania urządzeń w sieci, który to dopuszcza aby urządzenie samo nawiązywało połączenie do komputera, np. w celu dosłania dodatkowych szczegółów dotyczących ich konfiguracji. FirewallD takie połączenia traktuje jak 'niezamówiony ruch&#8217;, nie potrafi ich śledzić i dropuje.

Testy z wykorzystaniem avahi-browse i tcpdump&#8217;a pokazały, że problemem jest blokowanie odpowiedzi zwrotnej generowanej przez skaner. Nie chciałem całkowicie wyłączać firewalld (a takie rozwiązanie krążą głównie po sieci), ale niestety nie udało mi się jak na razie znaleźć eleganckiego rozwiązania.

Nieeleganckie rozwiązanie:

<pre class="wp-block-preformatted"># firewall-cmd --zone=public --add-rich-rule='rule family="ipv4" source address="&lt;tutaj-wpisz-ip-skanera&gt;" accept'
# firewall-cmd --runtime-to-permanent</pre>

Zrzuty z analizy:

Start VueScan z poziomu tcpdump&#8217;a

<pre class="wp-block-preformatted">[szydell@laPtak ~]$ sudo tcpdump src or dst 192.168.88.247
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on wlp3s0, link-type EN10MB (Ethernet), capture size 262144 bytes
23:42:09.219064 IP 192.168.88.247.mdns &gt; laPtak.53278: 0<em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306) 23:42:09.219160 ARP, Request who-has 192.168.88.247 tell laPtak, length 28 23:42:09.317428 ARP, Reply 192.168.88.247 is-at 9c:ae:d3:12:68:97 (oui Unknown), length 28 <strong>23:42:09.317454 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556 23:42:09.420803 IP </strong>192.168.88.247.mdns &gt; laPtak.53278: 0</em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306)
<strong>23:42:09.420892 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556</strong>
23:42:09.731152 IP 192.168.88.247.mdns &gt; laPtak.53278: 0<em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306) 23:42:09.731245 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556 23:42:10.046472 IP 192.168.88.247.mdns &gt; laPtak.53278: 0</em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306)
23:42:10.046562 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556
23:42:10.344299 IP 192.168.88.247.mdns &gt; laPtak.53278: 0<em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306) 23:42:10.344386 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556 23:42:10.649270 IP 192.168.88.247.mdns &gt; laPtak.53278: 0</em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306)
23:42:10.649358 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556
23:42:10.956049 IP 192.168.88.247.mdns &gt; laPtak.53278: 0<em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306) 23:42:10.956131 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556 23:42:11.263326 IP 192.168.88.247.mdns &gt; laPtak.53278: 0</em>- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306)
23:42:11.263416 IP laPtak &gt; 192.168.88.247: ICMP host laPtak unreachable - admin prohibited, length 556
23:42:11.569615 IP 192.168.88.247.mdns &gt; laPtak.53278: 0*- [4q],,, 4/0/10 PTR EPSON XP-640 Series._printer._tcp.local., PTR EPSON XP-640 Series._pdl-datastream._tcp.local., PTR EPSON XP-640 Series._scanner._tcp.local., PTR EPSON XP-640 Series._uscan._tcp.local. (1306)
23:42:14.288668 ARP, Request who-has laPtak tell 192.168.88.247, length 28
23:42:14.288694 ARP, Reply laPtak is-at 34:e1:2d:ee:21:18 (oui Unknown), length 28</pre>

avahi-browse

<pre class="wp-block-preformatted">[root@laPtak ~]#  avahi-browse -rt _scanner._tcp
+ wlp3s0 IPv6 EPSON XP-640 Series                           _scanner._tcp        local
+ wlp3s0 IPv4 EPSON XP-640 Series                           _scanner._tcp        local
= wlp3s0 IPv4 EPSON XP-640 Series                           _scanner._tcp        local
   hostname = [EPSON126897.local]
   address = [192.168.88.247]
   port = [1865]
   txt = ["note=" "scannerAvailable=1" "UUID=cfe92100-67c4-11d4-a45f-9caed3126897" "mdl=XP-640 Series" "mfg=EPSON" "adminurl=http://EPSON126897.local.:80/PRESENTATION/BONJOUR" "ty=EPSON XP-640 Series" "txtvers=1"]
<strong>Failed to resolve service 'EPSON XP-640 Series' of type '_scanner._tcp' in domain 'local': Timeout reached</strong>
</pre>