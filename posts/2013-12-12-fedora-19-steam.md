---
title: 'fedora 19: steam'
author: szydell
type: post
date: 2013-12-12T14:12:35+00:00
url: /post/2013/fedora-19-steam/
categories:
  - Aplikacje
  - Linux
tags:
  - Fedora
  - Fedora 19
  - nvidia
  - steam

---
W testowym repozytorium pakietów rpmfusion jest przygotowany dla fedory steam.

(dodawanie opisane we wpisie: <a title="Fedora 19: instalacja driverów nvidia" href="https://marcin.szydelscy.pl/post/2013/fedora-19-instalacja-driverow-nvidia/" target="_blank">https://marcin.szydelscy.pl/post/2013/fedora-19-instalacja-driverow-nvidia/</a>)

<pre>yum -y --enablerepo=rpmfusion-nonfree-updates-testing install steam</pre>

UWAGA! Jeżeli pracujemy na systemie 64bit należy jeszcze:

<pre>yum --enablerepo=rpmfusion-nonfree-updates-testing install akmod-nvidia xorg-x11-drv-nvidia-libs.i686</pre>

Steam jest aplikacją 32-bitową i wymaga doinstalowania odpowiednich wersji driverów.

(info o steamie specjalnie przygotowanym dla fedory znalazłem tutaj: <a title="Nocny Pingwin" href="http://nocnypingwin.blogspot.com/2013/11/klient-steam-w-repozytoriach-fedory.html" target="_blank">http://nocnypingwin.blogspot.com/2013/11/klient-steam-w-repozytoriach-fedory.html</a>)