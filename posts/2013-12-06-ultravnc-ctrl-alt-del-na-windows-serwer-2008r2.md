---
title: UltraVNC – ctrl-alt-del na windows serwer 2008R2
author: szydell
type: post
date: 2013-12-06T15:05:13+00:00
url: /post/2013/ultravnc-ctrl-alt-del-na-windows-serwer-2008r2/
categories:
  - VNC
tags:
  - ctrl-alt-del
  - del
  - delete
  - UltraVNC
  - VNC
  - windows
  - windows 7
  - windows server
  - windows server 2008
  - windows server 2008R2

---
Po zainstalowaniu UltraVNC na Windows Server 200R2, ale też Windows 7 i zapewne na kilku innych wersjach, nie działa możliwość wysłania CTRL-ALT-DEL, co uniemożliwia zalogowanie się. Trzeba w gpedit.msc zmienić konkretny klucz uprawnień. Po kolei:

wersja ang:

  1. &#8222;Start menu&#8221; -> &#8222;Execute&#8221; &#8211; > gpedit.msc
  2. &#8222;Computer Configuration&#8221; -> &#8222;Administrative Templates&#8221; -> &#8222;Windows Components&#8221; -> &#8222;Windows Logon Options&#8221;
  3. &#8222;Disable or enable software Secure Attention Sequence&#8221;
  4. Enable, Options: Service
  5. Apply

wersja pl:

  1. &#8222;Menu Start&#8221; -> Uruchom -> gpedit.msc
  2. &#8222;Konfiguracja komputera&#8221; -> &#8222;Szablony administracyjne&#8221; -> &#8222;Składniki systemu Windows&#8221; -> &#8222;Opcje logowania systemu Windows&#8221;
  3. &#8222;Włącz lub wyłącz sekwencję SAS dla oprogramowania&#8221;
  4. Włączone, Opcje: Usługi
  5. Zastosuj

Oczywiście zakładam, że UltraVNC działa jako usługa.

Rozwiązanie wykombinowane z wątku: <a title="send-ctrl-alt-del-not-working-with-ultravnc" href="http://community.spiceworks.com/topic/118546-send-ctrl-alt-del-not-working-with-ultravnc" target="_blank">http://community.spiceworks.com/topic/118546-send-ctrl-alt-del-not-working-with-ultravnc</a>