---
title: PLNc – testy minerów na sprzętach w miarę domowych
author: szydell
type: post
date: 2014-03-23T20:17:01+00:00
url: /post/2014/plnc-testy-minerow-na-sprzetach-w-miare-domowych/
categories:
  - Kryptowaluty
tags:
  - BITCOIN
  - koparka
  - Litecoin
  - miner
  - PLC
  - PLNc
  - PLNcoin

---
Niedawno pojawiła się nowa kryptowaluta &#8211; PLNc, czyli 'Polski BITCOIN&#8217;., chociaż w zasadzie powinno się pisać, że to nasz Litecoin.

Z czystej ciekawości postanowiłem sprawdzić sprzęt, którym dysponuję, no i w zasadzie potwierdziłem to, co można było przewidzieć. Ani dość mocny desktop, ani znośny laptop, ani pierwszy lepszy serwer ni cholery nie nadaje się do kopania 🙂

Wyniki poniżej Mhash/s są w zasadzie niesatysfakcjonujące. Także jak się chce zarobić &#8211; trzeba wykosztować się na koparkę.

Garść statystyk:

<table>
  <tr>
    <td>
      CPU/GPU
    </td>
    
    <td>
      soft
    </td>
    
    <td>
      sysop
    </td>
    
    <td>
      khash/s
    </td>
  </tr>
  
  <tr>
    <td>
      GeForce 650 Ti
    </td>
    
    <td>
      cudaminer 2014-02-28
    </td>
    
    <td>
      W7 x64
    </td>
    
    <td>
      160
    </td>
  </tr>
  
  <tr>
    <td>
      intel i5-2500 @ 3.30GHz
    </td>
    
    <td>
      pooler-cpuminer 2.3.3.
    </td>
    
    <td>
      W7 x64
    </td>
    
    <td>
      40
    </td>
  </tr>
  
  <tr>
    <td>
      intel i5 M430 @ 2.27GHz
    </td>
    
    <td>
      pooler-cpuminer 2.3.3.
    </td>
    
    <td>
      W7 x64
    </td>
    
    <td>
      20
    </td>
  </tr>
  
  <tr>
    <td>
      intel xeon X3440 @ 2.53Ghz
    </td>
    
    <td>
      pooler-cpuminer 2.3.3.
    </td>
    
    <td>
      W2k8 R2 x64
    </td>
    
    <td>
      20
    </td>
  </tr>
  
  <tr>
    <td>
      GeForce 330M
    </td>
    
    <td>
      cudaminer 2014-02-28
    </td>
    
    <td>
      W7 x64
    </td>
    
    <td>
      10
    </td>
  </tr>
</table>