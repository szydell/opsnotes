---
title: '[Fedora 28] Instalacja Kimchi 2.5'
author: szydell
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=622
featured_image: /wp-content/uploads/2019/04/kimchi.png
categories:
  - Aplikacje
  - Fedora
  - VNC

---
Repozytorium Fedory zawiera obecnie Kimchi w wersji 1.5. To dość archaiczne wydanie. Kimchi udostępnia jednak paczki. Także do Fedory. Niestety do wersji 25.  
Oczywiście jak to zwykle bywa coś z tego powodu nie działa.

Instrukcja instalacji:

<pre class="wp-block-code"><code>dnf install https://github.com/kimchi-project/kimchi/releases/download/2.5.0/kimchi-2.5.0-0.fc25.noarch.rpm https://github.com/kimchi-project/kimchi/releases/download/2.5.0/wok-2.5.0-0.fc25.noarch.rpm</code></pre>

Bugfixing:

<pre class="wp-block-code"><code>sed -i 's/passwd/system-auth/' /usr/lib/python2.7/site-packages/wok/auth.py</code></pre>

<pre class="wp-block-code"><code>firewall-cmd --add-service=wokd --permanent
firewall-cmd --reload</code></pre>

<pre class="wp-block-code"><code>semanage port -a -t http_port_t -p tcp 8001</code></pre>

<pre class="wp-block-code"><code>#server {
#    listen 0.0.0.0:8000;
#    rewrite ^/(.*)$ https://$host:8001/$1 redirect;
#}</code></pre>