---
title: MBR w Windows + OCZ Revodrive
author: szydell
type: post
date: 2014-07-09T21:52:30+00:00
url: /post/2014/mbr-w-windows-ocz-revodrive/
categories:
  - Windows
tags:
  - fix mbr
  - MBR
  - mbrfix
  - windows

---
Jestem szczęśliwym posiadaczem OCZ Revodrive. Świetna sprawa&#8230; dopóki coś się nie schrzani 😉

Strasznie sobie namieszałem z MBR. Trzeba było go odzyskać&#8230;

  1. Odpal W7 z płytki instalacyjnej.
  2. Znajdź opcję napraw system.
  3. Załaduj drivery, których kopia jest tu: [OCZ_x64][1] (wrzucamy na pendraka, rozpakowujemy)
  4. Zacznie być widoczny dysk, ale nadal nie będzie widać systemu. Wyszukanie systemu przy użyciu narzędzia **bootrec** działa, ale naprawa mbr już nie.
  5. Tak więc następne narzędzie, które trzeba mieć na pendrive &#8211; [mbrfix][2]
  6. mbrfix z opcją 'fixmbr&#8217; i działa.

&nbsp;

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2014/07/OCZ_x64.7z
 [2]: https://marcin.szydelscy.pl/wp-content/uploads/2014/07/mbrfix.7z