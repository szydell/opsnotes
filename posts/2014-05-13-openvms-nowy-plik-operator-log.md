---
title: 'OpenVMS: Nowy plik operator.log'
author: szydell
type: post
date: 2014-05-13T09:28:57+00:00
url: /post/2014/openvms-nowy-plik-operator-log/
categories:
  - OpenVMS
tags:
  - Alpha
  - DCL
  - OpenVMS
  - operator.log

---
Żeby stworzyć nowy plik z logiem, najprościej jest stworzyć sobie coma z następującymi informacjami:

<pre>$ SET PROCESS/PRIVILEGES=OPER
$ DEFINE SYS$COMMAND OPA0:
$ REPLY/ENABLE
$ REPLY/LOG
$ DEASSIGN SYS$COMMAND 
</pre>