---
title: Ustawienie strefy czasowej (systemd).
author: szydell
type: post
date: 2020-06-05T07:52:00+00:00
url: /post/2020/ustawienie-strefy-czasowej-systemd/
categories:
  - Aplikacje
  - Fedora
  - HypriotOS
  - Linux
tags:
  - systemd
  - timedatectl
  - timezone

---
W paczce systemd możemy znaleźć narzędzie do zarządzania ustawieniami czasu w Linuksie. Narzędzie pokazuje obecny status, pozwala też w prosty status ustawić TZ.

Weryfikacja:

<pre class="wp-block-preformatted">$ timedatectl
Local time: Sat 2020-05-23 15:39:25 CEST
Universal time: Sat 2020-05-23 13:39:25 UTC
RTC time: Sat 2020-05-23 13:39:25
Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: yes
NTP service: active
RTC in local TZ: no</pre>

Chcę, żeby serwer pracował zgodnie z czasem polskim. Komenda 'timedatectl list-timezones&#8217; pozwala znaleźć odpowiadającą nam strefę czasową, czyli w tym przypadku 'Europe/Warsaw&#8217;.

  
No to ustawiamy i weryfikujemy:

<pre class="wp-block-preformatted">$ <strong>sudo timedatectl set-timezone 'Europe/Warsaw'</strong>
$ <strong>timedatectl</strong>
Local time: Sat 2020-05-23 15:41:38 CEST
Universal time: Sat 2020-05-23 13:41:38 UTC
RTC time: Sat 2020-05-23 13:41:38
Time zone: Europe/Warsaw (CEST, +0200)
System clock synchronized: yes
NTP service: active
RTC in local TZ: no</pre>

Chcesz automatycznie synchronizować czas na swoim serwerze? Rzuć okiem na [artykuł opisujący jak skonfigurować ntp z użyciem serwisu systemd-timesyncd][1].

 [1]: https://marcin.szydelscy.pl/?p=761