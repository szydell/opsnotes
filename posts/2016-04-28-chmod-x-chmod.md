---
title: chmod -x chmod
author: szydell
type: post
date: 2016-04-28T11:44:33+00:00
url: /post/2016/chmod-x-chmod/
categories:
  - Linux
tags:
  - chmod
  - ld.so
  - ldd
  - tips

---
Jak naprawić system gdy się jest geniuszem zła i wyda się komendę chmod -x chmod?  
<?prettify linenums=true?>

<pre class="prettyprint">64bit:
/lib64/ld-linux-x86-64.so.2 /bin/chmod +x /bin/chmod
32bit:
/lib/ld-linux.so.2 /bin/chmod +x /bin/chmod</pre>

Brak pewności które użyć?  
Sprawdź:  
<?prettify linenums=true?>

<pre class="prettyprint">[user@linux katalog]$ ldd /bin/chmod
        linux-vdso.so.1 =&gt;  (0x00007ffd263fd000)
        libc.so.6 =&gt; /lib64/libc.so.6 (0x0000003f7c400000)
        /lib64/ld-linux-x86-64.so.2 (0x0000003f7c000000)</pre>