---
title: VIM – wcięcie na 4 spacje
author: szydell
type: post
date: 2017-05-03T20:56:32+00:00
url: /post/2017/vim-wciecie-na-4-spacje/
categories:
  - Aplikacje
tags:
  - vim

---
Wkurza mnie niezmiernie to, że VIM standardowe wcięcie robione tab&#8217;em ma ustawione na szerokość 8 spacji.

Na szczęście łatwo to zmienić. W pliku ~/.vimrc ustaw:

<?prettify linenums=true?>

<pre class="prettyprint">set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab</pre>