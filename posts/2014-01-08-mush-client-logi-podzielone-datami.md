---
title: 'MUSH Client: logi podzielone datami'
author: szydell
type: post
date: 2014-01-08T10:18:51+00:00
url: /post/2014/mush-client-logi-podzielone-datami/
categories:
  - MUSH Client
tags:
  - logowanie
  - logrotate
  - MUSH Client
  - timer

---
W MUSH Cliencie można w prosty sposób zrobić sobie namiastkę logrotate. Odpowiednią konfiguracją i jednym timerem zmusimy klienta, aby logować do pliku z aktualną datą w nazwie.

  1. Poprawna konfiguracja globalna 
      * File &#8211; Global preferences&#8230;
      * Zakładka 'Logging&#8217;
      * Wybieramy katalog, w którym chcemy przechowywać logi
      * Zaznaczamy 'Append to log files&#8217;
      * Odznaczamy 'Confirm when closing log file&#8217;
      * OK
  2. Poprawna konfiguracja świata 
      * File &#8211; World properties&#8230;
      * General &#8211; Logging
      * W polu 'Automatically log to this file:&#8217; wpisujemy:  
        (dla logów kolorowych/html)</p> 
        <pre>%Llog-%N-%d-%m-%Y.html</pre>
        
        (dla logów txt)
        
        <pre>%Llog-%N-%d-%m-%Y.txt</pre>

  3. Dodajemy timer 
      * File &#8211; World properties&#8230;
      * General &#8211; timers
      * Skopiuj do schowka zawartość tego pliku: [log_timer.txt][1]
      * Przycisk: 'Paste&#8217;
      * OK

&nbsp;

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2014/01/log_timer.txt