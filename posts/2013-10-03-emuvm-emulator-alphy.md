---
title: EmuVM – Emulator Alphy
author: szydell
type: post
date: 2013-10-03T10:32:50+00:00
url: /post/2013/emuvm-emulator-alphy/
categories:
  - Alpha
tags:
  - Alpha
  - emulator
  - EmuVM
  - FreeAXP

---
Kolejny (po <a title="FreeAXP - Alpha emulator" href="https://marcin.szydelscy.pl/post/2013/freeaxp-darmowa-wirtualna-alpha/" target="_blank">FreeAXP</a>) testowany przeze mnie emulator systemu opartego o procesory Alpha to EmuVM.

Strona produktu: <a title="EmuVM" href="http://www.emuvm.com/alphavm_free.php" target="_blank">http://www.emuvm.com/alphavm_free.php</a>

W porównaniu z FreeAXP darmowa wersja przede wszystkim pozwala wykorzystać 512MB ram.

Dodatkowo, co z mojego punktu widzenia jest niesamowicie ważne, emulator ten działa na Linuksie. Najnowsze wersje tylko na 64bit, ale to w zasadzie dobrze 🙂

Z dobrych wieści dotyczących emulacji, ważne są wyniki benchmarków. Według producenta emulator odpalony na współczesnym x86_64, zjada najmocniejsze Alphy pod względem wydajności. Niedługo będę weryfikował te informacje i pewnie uda mi się wrzucić wyniki testów w innym wpisie.