---
title: rm z pipe’m, czyli jak posprzątać po przypadkowym wypakowaniu archiwum nie tam gdzie się chciało
author: szydell
type: post
date: 2013-10-29T09:21:19+00:00
url: /post/2013/rm-z-pipem-czyli-jak-posprzatac-po-przypadkowym-wypakowaniu-archiwum-nie-tam-gdzie-sie-chcialo/
categories:
  - bash
tags:
  - bash
  - pipe
  - rm
  - tar
  - xargs

---
Czasem w ramach roztargnienia zdarza się nam narozrabiać. Ja dzisiaj rozpakowałem sobie archiwum w miejsce, w którym mam sporo innych plików&#8230; Zrobił się konkretny bajzel, także trzeba było posprzątać.

Pierwsze co mi przyszło do głowy, to wyświetlić zawartość archiwum, użyć | rm -rf i tyle, ale okazuje się, że nie da się tak przekazać listy plików do rm i koniec. Jak zawsze z pomocą przyszedł Pan Gugiel, a właściwe rozwiązanie problemu to:

<pre>tar -tf nazwaarchiwum.tar | xargs rm -f</pre>

Proste.