---
title: Czas do zablokowania ekranu, Windows 10
author: szydell
type: post
date: 2021-07-21T14:44:00+00:00
url: /post/2021/czas-do-zablokowania-ekranu-windows-10/
featured_image: /wp-content/uploads/2021/07/Windows-10-logo-604x270.jpg
categories:
  - Windows
tags:
  - blokada ekranu
  - regedit
  - windows 10

---
To totalne wariactwo, ale okazuje się, że standardowo w Windows 10 nie ma opcji pozwalającej na ustawienie po jakim czasie ma się zablokować ekran. Na szczęście da się ją włączyć w rejestrze.  
Jak więc ustawić czas do zablokowania ekranu? Odpalamy regedit&#8217;a i szukamy klucza:

<pre class="wp-block-preformatted">HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Power\PowerSettings\7516b95f-f776-4464-8c53-06167f40cc99\8EC4B3A5-6868-48c2-BE75-4F3044BE88A7</pre>

Następnie w parametr Attributes ustawiamy na 2. Standardowo będzie tam 1. W rezultacie odblokuje nam się opcja 'Limit czasu wyłączenia ekranu blokady konsoli&#8217;. Znaleźć ją można też bardzo prosto, ścieżka to:

<pre class="wp-block-preformatted">Panel sterowania -> Sprzęt i dźwięk -> Opcje zasilania -> Edytowanie ustawień planu -> Zmień zaawansowane ustawienia zasilania</pre>

W nowo otwartym okienku, szukamy na liście rozwijanej:

<pre class="wp-block-preformatted">Ekran -> Limit czasu wyłączenia ekranu blokady konsoli</pre>

I wreszcie możemy ustawić sobie taki czas, jaki nam odpowiada.