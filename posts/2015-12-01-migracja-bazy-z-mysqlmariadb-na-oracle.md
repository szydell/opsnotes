---
title: Migracja bazy z MySQL/MariaDB na Oracle
author: szydell
type: post
date: 2015-12-01T14:00:16+00:00
url: /post/2015/migracja-bazy-z-mysqlmariadb-na-oracle/
categories:
  - MariaDB
  - Oracle
tags:
  - MariaDB
  - MySQL
  - mysqldump
  - Oracle
  - RazorSQL

---
Pozornie proste zadanie okazało się być wyjątkowo upierdliwe.

  1. Instrukcja ze strony Oracle (z wykorzystaniem SQL Developera) wywołała u mnie myśli samobójcze
  2. Wykorzystanie mysqldump&#8217;a z opcją &#8221; &#8211;compatible=oracle&#8221; wygenerowało plik, którego ani nie udało mi się zaczytać do Oracle 11g ani z powrotem do MariaDB

Musiałem ratować się inaczej&#8230;

Z pomocą w takich sytuacjach przychodzi <a href="http://www.razorsql.com" target="_blank">RazorSQL</a>. Narzędzie (niestety płatne), ale na szczęście z 30-to dniową wersją ewaluacyjną. RazorSQL potrafi przenieść dane bezpośrednio między różnymi systemami zarządzania bazą danych.  
Testowałem tę opcję z MariaDB w wersji 5.5 i Oracle w wersji 11g XE.

Ważna uwaga. Przy przenoszeniu 'ciut większej&#8217; bazy, RazorSQL będzie zgłaszał błąd przekroczenia Heap Size Java.  
Należy wtedy zedytować plik uruchamiający: razorsql.sh, a dokładniej parametr -Xmx. W przykładzie poniżej ograniczamy ilość dostępnej dla RazoraSQL pamięci do 6GB. Standardowo to tylko kilkaset MB.  
razorsql.sh po poprawce:  
`<br />
#!/bin/sh<br />
CWD=$(dirname "$0")<br />
CMD="$CWD/razorsql.jar"<br />
$CWD/jre/bin/java -Xms64M <span style="text-decoration: underline;"><strong>-Xmx6G</strong></span> -client -jar ${CMD}<br />
RC=$?<br />
if [ ${RC} != 0 ]; then<br />
echo "Error returned code found. Retrying . . ."<br />
$CWD/jre/bin/java -Xms64M -Xmx640M -client -jar ${CMD}<br />
RC2=$?<br />
if [ ${RC2} != 0 ]; then<br />
echo "Trying local JRE . . ."<br />
java -Xms64M -Xmx640M -client -jar ${CMD}<br />
fi<br />
fi<br />
`