---
title: Azure VPN point-to-site, Fedora 28
author: szydell
type: post
date: 2018-08-08T19:18:30+00:00
draft: true
url: /?p=575
categories:
  - Aplikacje
  - Azure
  - Cloud
  - Linux
tags:
  - Fedora 28
  - openswan

---
<li style="list-style-type: none;">
  <ol>
    <li>
      Instalacja pakietów<br /> <!--?prettify linenums=true?--></p> 
      
      <pre class="prettyprint">$ sudo yum install strongswan-libipsec</pre>
      
      <p>
        (zainstalowało mi 2 pakiety)</li> 
        
        <li>
          Generujemy klucze CA<br /> <!--?prettify linenums=true?--></p> 
          
          <pre class="prettyprint">$ strongswan pki --gen --outform pem &gt; caKey.pem
$ strongswan pki --self --in caKey.pem --dn "CN=VPN CA" --ca --outform pem &gt; caCert.pem</pre>
        </li>
        
        <li>
          Generujemy base64 dla Azure (bo w takim oto formacie przyjmują klucz CA)<br /> <!--?prettify linenums=true?--></p> 
          
          <pre class="prettyprint">$ openssl x509 -in caCert.pem -outform der | base64 -w0</pre>
          
          <p>
            (nie znikamy tego okienka, wynik potrzebny jest w następnym punkcie)</li> 
            
            <li>
              W portalu Azure, na zakładce naszego VPNa szukamy &#8222;Point-to-site configuration&#8221;. Wchodzimy do środka i wypełniamy: <ul>
                <li>
                  Address pool: podsieć do której chcemy się dostać po połączeniu VPN&#8217;em (musi być nowa). Np. 10.1.3.0/24
                </li>
                <li>
                  Root certificates <ul>
                    <li>
                      Name: <dowolna_nazwa> np. FedoraCert
                    </li>
                    <li>
                      Public certificate data: tu wklejamy base64 przygotowane z caCert.pem
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            
            <li>
              Po zapisaniu certyfikatu (co jak wszystko w Azure trochę trwa) ściągamy sobie pliczek dostępny pod przyciskiem 'Download VPN Client&#8217;.
            </li>
            <li>
              Wracamy do konsoli, żeby przygotować swój certyfikat użytkownika (można taki wygenerować dla każdego usera, który ma korzystać z VPN&#8217;a).<br /> <!--?prettify linenums=true?--></p> 
              
              <pre class="prettyprint">$ PASSWORD="password"
$ USER="client"
$ strongswan pki --gen --outform pem &gt; "${USER}Key.pem"
$ strongswan pki --pub --in "${USER}Key.pem" | strongswan pki --issue --cacert caCert.pem --cakey caKey.pem --dn "CN=${USER}" --san "${USER}" --flag clientAuth --outform pem &gt; "${USER}Cert.pem"</pre>
            </li>
            
            <li>
              Pakujemy certyfikat w paczkę p12, tworzymy katalog i kopiujemy do niego cert p12<br /> <!--?prettify linenums=true?--></p> 
              
              <pre class="prettyprint">$ openssl pkcs12 -in "${USER}Cert.pem" -inkey "${USER}Key.pem" -certfile caCert.pem -export -out "${USER}.p12" -password "pass:${PASSWORD}"
$ sudo cp client.p12 /etc/strongswan/ipsec.d/private</pre>
            </li>
            
            <li>
              Ściągnięty wcześniej z Azure plik <nazwaVPNa>.zip zawiera trochę więcej niż potrzebujemy, także będziemy wybredni:<br /> <!--?prettify linenums=true?--></p> 
              
              <pre class="prettyprint">$ sudo unzip -j FedoraVPN.zip Generic/VpnServerRoot.cer -d /etc/strongswan/ipsec.d/cacerts/cacerts</pre>
              
              <p>
                Jak ktoś ma ochotę, to można zweryfikować czy certyfikat jest ok komendą:<br /> <!--?prettify linenums=true?-->
              </p>
              
              <pre class="prettyprint">$ sudo openssl x509 -inform der -in /etc/strongswan/ipsec.d/cacerts/VpnServerRoot.cer -text -noout</pre>
            </li>
            
            <li>
              Z załączonego do zipa pliku konfiguracyjnego wyciągamy jeszcze interesujący nas parametr, czyli domenę pod którą nasz Azure VPN słucha. Jeżeli przypisaliśmy do VPNa publiczny, dynamiczny adres, prawdopodobnie nie chcemy próbować się łączyć po IP.<br /> <!--?prettify linenums=true?--></p> 
              
              <pre class="prettyprint">$ unzip -p FedoraVPN.zip Generic/VpnSettings.xml | grep VpnServer</pre>
              
              <p>
                Wynik podstaw do zmiennych right i rightid w następnym kroku.</li> 
                
                <li>
                  Edytujemy plik plik konfiguracyjny ipsec z pakietu strongswan <pre class="prettyprint">$ sudo vim /etc/strongswan/ipsec.conf</pre>
                  
                  <p>
                    Zawartość konfiga:
                  </p>
                  
                  <pre>config setup

conn azure
  keyexchange=ikev2
  type=tunnel
  leftfirewall=yes
  left=%any
  leftauth=eap-tls
  leftid=%client
  right=azuregateway-00112233-4455-6677-8899-aabbccddeeff-aabbccddeeff.cloudapp.net # adres dns Azure VPN
  rightid=%azuregateway-00112233-4455-6677-8899-aabbccddeeff-aabbccddeeff.cloudapp.net # % + adres dns Azure APN
  rightsubnet=0.0.0.0/0
  leftsourceip=%config
  auto=add</pre>
                </li>
                
                <li>
                  W pliku /etc/strongswan/ipsec.secrets podajemy:<br /> : P12 client.p12 'password&#8217; #nazwa paczki.p12  /etc/strongwan/ipsec.d/private
                </li></ol> </li> </ol> 
                
                <p>
                  Notatka stworzona na podstawie:
                </p>
                
                <ul>
                  <li>
                    <a href="https://serverfault.com/questions/840920/how-connect-a-linux-box-to-an-azure-point-to-site-gateway">https://serverfault.com/questions/840920/how-connect-a-linux-box-to-an-azure-point-to-site-gateway</a>
                  </li>
                  <li>
                    <a href="https://danielpocock.com/ussing-ecc-ecdsa-in-openssl-and-strongswan-fedora">https://danielpocock.com/ussing-ecc-ecdsa-in-openssl-and-strongswan-fedora</a>
                  </li>
                </ul>