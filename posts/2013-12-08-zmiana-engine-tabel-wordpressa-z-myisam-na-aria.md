---
title: Zmiana engine tabel WordPressa z MyISAM na ARIA
author: szydell
type: post
date: 2013-12-07T23:16:44+00:00
url: /post/2013/zmiana-engine-tabel-wordpressa-z-myisam-na-aria/
categories:
  - MariaDB
tags:
  - ARIA
  - db engine
  - MariaDB
  - MariaDB 10
  - MyISAM
  - MySQL

---
Komplet komend SQL. Oczywiście identycznie można zmienić engine każdej innej tabeli w bazie.

Opcja TRANSACTIONAL=1 oznacza, że włączamy dla tabeli tryb crash-safe.

<pre>ALTER TABLE `wp_commentmeta` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_comments` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_links` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_options` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_postmeta` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_posts` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_terms` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_term_relationships` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_term_taxonomy` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_usermeta` ENGINE=`ARIA` TRANSACTIONAL=1;
ALTER TABLE `wp_users` ENGINE=`ARIA` TRANSACTIONAL=1;</pre>

Niektóre pluginy mogą założyć sobie dodatkowe tabele. Zmieniamy je analogicznie.