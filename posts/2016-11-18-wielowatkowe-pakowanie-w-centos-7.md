---
title: Wielowątkowe pakowanie w Centos 7
author: szydell
type: post
date: 2016-11-18T18:00:00+00:00
url: /post/2016/wielowatkowe-pakowanie-w-centos-7/
categories:
  - Linux
tags:
  - centos 7
  - gzip
  - mc
  - midnight commander
  - PATH
  - pigz
  - RHEL 7

---
Red Hat / CentOS niestety nie rozpieszczają pod względem dostępności fajnych zabawek w standardowym repozytorium  
Jedyne co znalazłem to 'pigz&#8217; czyli rozwiązanie dające tę samą funkcjonalność co gzip, ale na wielu core&#8217;ach.

Trzeba oczywiście doinstalować:  
<!--?prettify linenums=true?-->

<pre class="prettyprint">yum install pigz</pre>

Niestety nie da się prosto odinstalować gzip&#8217;a i zastąpić go pigz&#8217;em. Ale zawsze można pokombinować 🙂

  1. Sprawdź jak wygląda twoja zmienna środowiskowa $PATH. Z root&#8217;a powinno to być coś w ten deseń:  
    <!--?prettify linenums=true?--></p> 
    
    <pre class="prettyprint">echo $PATH
/usr/local/sbin:/sbin:/bin/:/usr/sbin:/usr/bin:/root/bin</pre>

  2. Zlokalizuj gdzie masz gzip&#8217;a. Standardowo jest w lokalizacji: /usr/bin/gzip
  3. Jak widać mamy niewielkie możliwości do podmienienia sobie gzipa z wykorzystaniem dowiązania symbolicznego, także zróbmy sobie linka w /sbin  
    <!--?prettify linenums=true?--></p> 
    
    <pre class="prettyprint">sudo ln -s /usr/bin/pigz /sbin/gzip</pre>

  4. Teraz jeżeli uruchomienie komendy 'gzip&#8217; bez ścieżki, odpali pigz&#8217;a.

Dla leniwców (albo kogoś kto nie ma roota) &#8211; można podmienić sobie komendę np. w menu Midnight Commandera.

W pliku: /etc/mc/mc.menu zamieniamy komendę 'gzip&#8217; na 'pigz&#8217;:  
Linia:  
<?prettify linenums=true?>

<pre class="prettyprint">tar cf - "$Pwd" | <strong>gzip</strong> -f9 &gt; "$tar.tar.gz" && \</pre>

po zmianie:  
<?prettify linenums=true?>

<pre class="prettyprint">tar cf - "$Pwd" | <strong>pigz</strong> -f9 &gt; "$tar.tar.gz" && \</pre>

voila