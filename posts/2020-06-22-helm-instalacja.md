---
title: Helm instalacja
author: szydell
type: post
date: 2020-06-22T18:22:43+00:00
url: /post/2020/helm-instalacja/
featured_image: /wp-content/uploads/2020/06/helm-305x270.png
categories:
  - DevOps
  - Kubernetes
tags:
  - helm

---
Niby wszystko opisane na <a rel="noreferrer noopener" href="https://helm.sh/docs/intro/install/" target="_blank">stronie</a>, ale jakoś nie do końca&#8230;

A co to ten Helm?  
To narzędzie do zarządzania 'Kubernetes charts&#8217;. A Charts to nic więcej jak definicje jak uruchamiać aplikacje na Kubernetesie. Taki system pakietów.

Repo Debian/Ubuntu x86_64:

<pre class="wp-block-preformatted">curl https://helm.baltorepo.com/organization/signing.asc | sudo apt-key add -
sudo apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm</pre>

Fedora &#8211; nie ma repozytorium 🙁  
Repo powyżej **nie działa** też na Raspbery HypriotOS. Brakuje pakietu dla architektury.  
Ale to nie problem.  
Fedora &#8211; Opcja 1 (wrzucenie binarki skryptem):

<pre class="wp-block-preformatted">$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ chmod 700 get_helm.sh
$ ./get_helm.sh</pre>

Fedora &#8211; Opcja 2 (snapd):

<pre class="wp-block-preformatted">(jeżeli nie masz snapd)
$ sudo dnf install snapd
$ sudo ln -s /var/lib/snapd/snap /snap
(instalacja helma)
$ sudo snap install helm --classic</pre>

HypriotOS &#8211; Opcja 1 (snapd):

<pre class="wp-block-preformatted">$ sudo apt install snapd
$ sudo snap install helm --classic
(uruchamianie)
$ /snap/bin/helm</pre>

HypriotOS &#8211; Opcja 2 (binarka):

  1. Ze strony z <a rel="noreferrer noopener" href="https://github.com/helm/helm/releases" target="_blank">releasami Helma </a>ściągnij sobie odpowiednią binarkę.
  2. Rozpakuj
  3. Przenieś plik 'helm&#8217; np. do /usr/local/bin