---
title: Synchro czasu ntp Linux (systemd)
author: szydell
type: post
date: 2020-06-02T07:23:00+00:00
url: /post/2020/synchro-czasu-ntp-linux-systemd/
categories:
  - Aplikacje
  - Fedora
  - HypriotOS
  - Linux
tags:
  - Debian
  - ntp
  - systemd
  - timesyncd

---
Kolejny już wpis o synchronizacji czasu ntp. O dziwo pozmieniało się od 2014 roku, kiedy cały dumny opisałem jak w [sposób nieprzesadzony synchronizować czas na Linuxach][1]. 😉

Ktoś mądrzejszy ode mnie wdrożył to o czym pisałem &#8211; debilizmem jest stawianie serwera ntpd, jeżeli nasz Linux ma być tylko klientem. Wdrożył w systemd, także dystrybucje które go używają, powinny mieć to rozwiązanie out of box (na pewno mają Fedora, Arch i HypriotOS).

No to po kolei. Sprawdzamy czy faktycznie posiadamy taki serwis:

<pre class="wp-block-preformatted">[szydell@laPtak ~]$ systemctl status systemd-timesyncd.service
● systemd-timesyncd.service - Network Time Synchronization
Loaded: loaded (/usr/lib/systemd/system/systemd-timesyncd.service; disabled; vendor preset: disabled)
Active: inactive (dead)
Docs: man:systemd-timesyncd.service(8)</pre>

Jest. Sprawdzamy czy nasz system nie korzysta już czasem z jakiegoś dostawcy czasu. Jeżeli korzysta, to wyłączamy. 

Chronyd (Fedora):

<pre class="wp-block-preformatted">$ <strong>sudo</strong> <strong>systemctl status chronyd</strong>
● chronyd.service - NTP client/server
Loaded: loaded (/usr/lib/systemd/system/chronyd.service; enabled; vendor preset: enabled)
Active: <strong><span style="color:#2b813d" class="has-inline-color">active (running) </span></strong>since Fri 2020-05-22 12:39:12 CEST; 59min ago
...

$ <strong>sudo systemctl disable chronyd.service --now</strong>
Removed /etc/systemd/system/multi-user.target.wants/chronyd.service.</pre>

Ntpd (HypriotOS/Debian):

<pre class="wp-block-preformatted">$ <strong>sudo systemctl status ntp</strong>
● ntp.service - Network Time Service
Loaded: loaded (/lib/systemd/system/ntp.service; enabled; vendor preset: enabled)
Active: <span style="color:#2b813d" class="has-inline-color"><strong>active (running)</strong></span> since Tue 2020-05-12 09:17:07 UTC; 1 weeks 3 days ago
...

$ <strong>sudo systemctl disable ntp --now</strong>
Synchronizing state of ntp.service with SysV service script with /lib/systemd/systemd-sysv-install.
Executing: /lib/systemd/systemd-sysv-install disable ntp
Removed /etc/systemd/system/multi-user.target.wants/ntp.service.

$ <strong>sudo apt purge ntp</strong>
Reading package lists… Done
Building dependency tree
Reading state information… Done
The following packages will be REMOVED:
ntp*
0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
After this operation, 1,848 kB disk space will be freed.
Do you want to continue? [Y/n]
(Reading database … 28459 files and directories currently installed.)
Removing ntp (1:4.2.8p12+dfsg-4) …
Processing triggers for man-db (2.8.5-2) …
(Reading database … 28404 files and directories currently installed.)
Purging configuration files for ntp (1:4.2.8p12+dfsg-4) …
Processing triggers for systemd (241-7~deb10u3+rpi1) …</pre>

Konfiguracja serwisu systemd-timesyncd leży w pliku /etc/systemd/timesyncd.conf.  
Ustaw NTP i FallbackNTP. Resztę parametrów można skasować. Wartości domyślne są ok.  
  
Przykładowy config (główne <a rel="noreferrer noopener" href="https://www.ntppool.org/zone/pl" target="_blank">serwery ntp z polski.</a> Awaryjne z zasobów Fedory i Debiana):

<pre class="wp-block-preformatted"># <strong>cat systemd/timesyncd.conf</strong>
[Time]
NTP=0.pl.pool.ntp.org 1.pl.pool.ntp.org 2.pl.pool.ntp.org 3.pl.pool.ntp.org
FallbackNTP=0.fedora.pool.ntp.org 1.fedora.pool.ntp.org 2.fedora.pool.ntp.org 3.fedora.pool.ntp.org 0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org</pre>

Uruchomienie:

<pre class="wp-block-preformatted">[szydell@laPtak ~]$ <strong>sudo systemctl enable systemd-timesyncd.service --now</strong>
Created symlink /etc/systemd/system/dbus-org.freedesktop.timesync1.service → /usr/lib/systemd/system/systemd-timesyncd.service.
Created symlink /etc/systemd/system/sysinit.target.wants/systemd-timesyncd.service → /usr/lib/systemd/system/systemd-timesyncd.service.

[szydell@laPtak ~]$ <strong>sudo timedatectl set-ntp true</strong></pre>

<div class="wp-block-coblocks-accordion">
  <div class="wp-block-coblocks-accordion-item">
  </div>
</div>

Efekty zmian można szybko zweryfikować:

<pre class="wp-block-preformatted">[szydell@laPtak ~]$ <strong>timedatectl</strong>
Local time: pią 2020-05-22 14:48:08 CEST
Universal time: pią 2020-05-22 12:48:08 UTC
RTC time: pią 2020-05-22 12:48:08
Time zone: Europe/Warsaw (CEST, +0200)
System clock synchronized: yes
NTP service: active
RTC in local TZ: no

[szydell@laPtak ~]$ <strong>timedatectl timesync-status</strong>
Server: 162.159.200.123 (0.pl.pool.ntp.org)
Poll interval: 1min 4s (min: 32s; max 34min 8s)
Leap: normal
Version: 4
Stratum: 3
Reference: A490853
Precision: 1us (-25)
Root distance: 11.786ms (max: 5s)
Offset: +2.133ms
Delay: 12.974ms
Jitter: 0
Packet count: 1
Frequency: -13,327ppm</pre>

 [1]: https://marcin.szydelscy.pl/post/2014/synchronizacja-czasu-linux-wersja-nieprzesadzona/