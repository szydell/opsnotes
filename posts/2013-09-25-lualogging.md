---
title: LuaLogging – moduł dostarczający proste api do tworzenia logów
author: szydell
type: post
date: 2013-09-25T12:20:47+00:00
url: /post/2013/lualogging/
categories:
  - Lua
tags:
  - logowanie
  - Lua
  - LuaLogging

---
Fajne do zastosowania narzędzie, rozszerzające język LUA o możliwość tworzenia usystematyzowanych logów. Potrafi zapisać log do pliku, konsoli, serwera sql, socketu, i wysłać mailem

Do znalezienia tutaj: <a title="LuaLogging" href="http://www.keplerproject.org/lualogging/" target="_blank">http://www.keplerproject.org/lualogging/</a>

&nbsp;