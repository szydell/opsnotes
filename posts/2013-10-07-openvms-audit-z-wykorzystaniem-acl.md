---
title: OpenVMS AUDIT z wykorzystaniem ACL
author: szydell
type: post
date: 2013-10-07T09:28:31+00:00
url: /post/2013/openvms-audit-z-wykorzystaniem-acl/
categories:
  - OpenVMS
tags:
  - ACL
  - Alpha
  - AUDIT
  - audyt
  - del
  - delete
  - OpenVMS
  - SECURITY

---
OpenVMS ma bardzo ciekawie rozwiązaną funkcjonalność audytowania. Istnieje możliwość zdefiniowania precyzyjnego logowania mnóstwa czynności. Zarówno globalnie, jak i lokalnie.

Interesowało mnie wyświetlenie na ekranie informacji o tym, że ktoś czyta jeden z plików tekstowych.

<pre>$ set security/ACL=(ALARM=SECURITY, ACCESS=READ) TEST.TXT</pre>

Takie ustawienie ACL spowoduje,  że przy próbie przeczytania pliku TEST.TXT na konsoli bezpieczeństwa pojawi się informacja o takim zdarzeniu.  
Oczywiście globalnie musimy mieć włączony audyt dla acl&#8217;i, ale jest to opcja włączona by default.

Sprawdzenie jakie opcje security ustawiliśmy dla danego pliku:

<pre>$ show security test.txt</pre>

Jeżeli chcemy, żeby informacja o dostępie do naszego TEST.TXT została nie tylko wyświetlona na konsoli, ale też zlogowana:

<pre>$ set security/ACL=(AUDIT=SECURITY, ACCESS=READ) TEST.TXT</pre>

Oczywiście można logować zdarzenia oparte nie tylko o próbę READ. Jest ich sporo, a przetestowane przeze mnie to:

  * READ
  * WRITE
  * EXECUTE
  * DELETE
  * CONTROL
  * FAILURE
  * SUCCESS

Oczywiście można je ze sobą mieszać.

<pre>READ+WRITE+DELETE</pre>

Kasowanie ustawień ACL z naszego pliku:

<pre>$ SET SECURITY/ACL/DELETE TEST.TXT</pre>

&nbsp;

&nbsp;