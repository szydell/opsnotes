---
title: Zarządzanie użytkownikami w OpenVMS – add username, modify i nie tylko
author: szydell
type: post
date: 2013-09-25T12:04:52+00:00
url: /post/2013/zarzadzanie-uzytkownikami-w-openvms-add-username-modify-i-nie-tylko/
categories:
  - OpenVMS
tags:
  - add username
  - adduser
  - Alpha
  - modify
  - OpenVMS

---
OpenVMS ma dość nietypową składnię przy zakładaniu nowych użytkowników.

Całość operacji można podsumować krótkim kodem:

`$ set process/privileges=all<br />
$ set default sys$system:<br />
$ run authorize<br />
UAF> show [group,*] /brief<br />
UAF> add username /uic=[group,member] /account=account<br />
UAF> modify /password=password<br />
UAF> modify username /device=sys$sysdevice<br />
UAF> modify username /directory=[directory]<br />
UAF> modify username /owner="Given Name"<br />
UAF> modify username /nopwdexpir /flag=nodisuser<br />
UAF> modify username /defprivileges=(...) /privileges=(...)<br />
UAF> modify username /flags=nodisuser<br />
UAF> exit<br />
$ create /directory /owner=[group,member] sys$sysdevice:[directory]<br />
` 

Czyli po kolei:

  1. `$ set process/privileges=all` &#8211; uzyskanie pełnych uprawnień
  2. `$ set default sys$system:` &#8211; przejście do katalogu sys$system:
  3. `$ run authorize` &#8211; uruchomienie narzędzia do zarządzania userami
  4. `UAF> show [group,*] /brief` &#8211; wyświetlenie krótkiej listy userów z 'grupy&#8217;
  5. `UAF> add username /uic=[group,member] /account=account` &#8211; dodajemy użytkownika 'username&#8217; o UIC [group,member]
  6. modyfikujemy mu konkretne flagi, przyznajemy uprawnienia itd

**Uwaga! Niezalecane jest kasowanie użytkowników. Jeżeli chcemy kogoś usunąć z systemu, lepiej jest wykonać:** `<strong>modify username /flags=nodisuser</strong><br />
`