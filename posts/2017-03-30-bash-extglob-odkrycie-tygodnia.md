---
title: bash extglob odkrycie tygodnia :)
author: szydell
type: post
date: 2017-03-30T12:36:44+00:00
url: /post/2017/bash-extglob-odkrycie-tygodnia/
categories:
  - bash
  - Linux
tags:
  - exclude
  - extglob
  - rm

---
extglob czyli odpalenie &#8222;regexów&#8221; wewnątrz basha. Przykładowo proste kasowanie z excludem

<?prettify linenums=true?>

<pre class="prettyprint">[szydell@example test]$ ls
aaa1.txt aaa2.txt aaa3.txt aaa4.txt aaa5.txt aaa6.txt aaa7.txt aaa8.txt aaa9.txt bbb1.txt bbb2.txt bbb3.txt bbb4.txt bbb5.txt bbb6.txt bbb7.txt bbb8.txt bbb9.txt test1.txt test2.txt test3.txt test4.txt test5.txt</pre>

Chcemy wywalić *.txt z pominięciem aaa5.txt

<?prettify linenums=true?>

<pre class="prettyprint">[szydell@example test]$ shopt -s extglob
[szydell@example test]$ rm !(aaa5).txt
[szydell@example test]$ ls
aaa5.txt</pre>