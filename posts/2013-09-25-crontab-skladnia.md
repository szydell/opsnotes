---
title: Crontab – składnia
author: szydell
type: post
date: 2013-09-25T13:11:17+00:00
url: /post/2013/crontab-skladnia/
categories:
  - Linux
tags:
  - cron
  - crontab

---
Notorycznie zapominam składni w plikach crona, także&#8230; ściągawka.

<pre>$ crontab -e
0 2 * * * /usr/bin/skrypt.sh</pre>

<table border="0">
  <tr valign="top">
    <th scope="col" align="left" valign="top">
      Pole
    </th>
    
    <th scope="col" align="left" valign="top">
      Dozwolone wartości
    </th>
  </tr>
  
  <tr valign="top">
    <td align="left" valign="top">
      Minuta
    </td>
    
    <td align="left" valign="top">
      0–59
    </td>
  </tr>
  
  <tr valign="top">
    <td align="left" valign="top">
      Godzina
    </td>
    
    <td align="left" valign="top">
      0–23
    </td>
  </tr>
  
  <tr valign="top">
    <td align="left" valign="top">
      Dzień miesiąca
    </td>
    
    <td align="left" valign="top">
      1–31
    </td>
  </tr>
  
  <tr valign="top">
    <td align="left" valign="top">
      Miesiąc
    </td>
    
    <td align="left" valign="top">
      1–12
    </td>
  </tr>
  
  <tr valign="top">
    <td align="left" valign="top">
      Dzień tygodnia
    </td>
    
    <td align="left" valign="top">
      0–7 (0 i 7 to niedziela)
    </td>
  </tr>
</table>

Przykładowo:

`0 2 3 4 *` oznacza, że cron wystartuje o 2:00 trzeciego dnia kwietnia. Niezależnie od tego jaki to będzie dzień tygodnia.

`*/5 * * * 1-5` oznacza, że wykonanie nastąpi co 5 minut, ale tylko od poniedziałku do piątku

`0 17,18 * * *` uruchomi job o 17:00 i 18:00, codziennie.