---
title: Automatyczna synchronizacja czasu w CentOS 6.5
author: szydell
type: post
date: 2014-05-31T14:02:39+00:00
url: /post/2014/automatyczna-synchronizacja-czasu-w-centos-6-5/
categories:
  - Linux
tags:
  - CentOS
  - ntp
  - ntpd
  - ntpdate
  - synchronizacja czasu

---
DEPRECATED &#8211; NIE UŻYWAĆ O ILE NIE POTRZEBUJESZ SERWERA DO USTAWIANIA CZASU W WIĘKSZEJ SIECI LOKALNEJ.

&nbsp;

SPRAWDŹ WPIS: <a title="automatyczna synchronizacja czasu" href="https://marcin.szydelscy.pl/post/2014/synchronizacja-czasu-linux-wersja-nieprzesadzona/" target="_blank">https://marcin.szydelscy.pl/post/2014/synchronizacja-czasu-linux-wersja-nieprzesadzona/</a>

&nbsp;

&nbsp;

Prosta usługa, banalna konfiguracja, a przy tym w zasadzie podstawa dla serwerów.

  1. Zaczynamy od wstępnego zsynchronizowania czasu naszej maszyny: 
    <pre>[root@centos /]# ntpdate tempus1.gum.gov.pl
</pre>

  2. Instalujemy pakiet ntp: 
    <pre>[root@centos /]# yum install ntp
</pre>

  3. edytujemy plik /etc/ntp.conf: 
      * (opcjonalnie) zmieniamy listę serwerów na: 
        <pre>server tempus1.gum.gov.pl
server tempus2.gum.gov.pl
</pre>
    
      * (opcjonalnie) dodajemy plik do logowania informacji z ntpd: 
        <pre>logfile /var/log/ntp.log
</pre>

  4. Uruchamiamy usługę: 
    <pre>[root@centos /]# service ntpd start
</pre>

  5. Sprawdzamy czy podłączyła się do serwerów: 
    <pre>[root@centos /]# ntpq -p
</pre>
    
    (opcjonalnie) weryfikujemy log usługi:
    
    <pre>[root@centos /]# type  /var/log/ntp.log
</pre>

  6. Dodajemy do 'autostartu&#8217;: 
    <pre>[root@centos /]# chkconfig ntpd on
</pre>

Gratulacje. Od teraz Twój serwer będzie miał zawsze aktualny czas.

Polecam zmianę listy serwerów czasu na podane wyżej. Są to oficjalne serwery Głównego Urzędu Miar, korzystające z czasu serwowanego przez zegar atomowy. Źródło jest solidne i godne zaufania. Oczywiście ustawione w standardowym konfigu poole centosa, też dadzą radę, ale ja tam wolę nasze. 🙂