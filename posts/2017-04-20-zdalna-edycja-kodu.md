---
title: Zdalna edycja kodu
author: szydell
type: post
date: 2017-04-20T22:26:50+00:00
url: /post/2017/zdalna-edycja-kodu/
categories:
  - VSCode
tags:
  - putty
  - rmate
  - ssh
  - zdalnie

---
Zazwyczaj pracuję na Windowsie. A piszę programiki i skrypty stricte dla Linuxa.  
VIMa kocham niezmiernie, ale są fajniejsze zabawki do takiej pracy. Na przykład Visual Studio Code, które mi wyjątkowo podpasowało.  
VSCode ma fajną wtyczkę, dzięki której można po ssh edytować pliki na zdalnej maszynie 'w locie&#8217;. Nie jest to może najwygodniejsza w konfiguracji metoda, ale z pomocą putty da się ogarnąć temat dość prosto 🙂  
Wtyczka korzysta z <a href="https://github.com/aurora/rmate" target="_blank" rel="noopener noreferrer">rmate</a>. Najrozsądniejsza wydaje mi się implementacja w bashu (ale jak ktoś lubi to jest też w RUBY, Pythonie, C i pewnie w kilku innych językach)

  1. Zainstaluj w VSCode dodatek <a href="https://marketplace.visualstudio.com/items?itemName=rafaelmaiolla.remote-vscode" target="_blank" rel="noopener noreferrer">Remote VSCode</a>:  
    `ctrl+p` wklej `ext install remote-vscode`
  2. Na zdalnej maszynie instalujemy 'rmate&#8217;:  
    `sudo wget -O /usr/local/bin/rmate https://raw.github.com/aurora/rmate/master/rmate<br />
chmod a+x /usr/local/bin/rmate`</p> <div>
      <em>Wydaje mi się, że można bez żadnego uszczerbku na fajności, zaciągnąć sobie skrypt na lokalnego usera. System wide nie powinno być konieczne (aczkolwiek nie testowałem).</em>
    </div>

  3. Zestaw w putty tunel do maszyny na której chcesz edytować pliki. 'Główna&#8217; strona standardowo, natomiast Connection->SSH->Tunnels ustaw tak:  
    [<img loading="lazy" decoding="async" class="aligncenter size-medium wp-image-397" src="https://marcin.szydelscy.pl/wp-content/uploads/2017/04/tunel_rmate-300x288.jpg" alt="" width="300" height="288" />][1]
  4. CTRL-C &#8222;Remote: Start server&#8221;, następnie w VSCode -> Naciśnij F1, wklej i ENTER
  5. Na zdalnej maszynie wydaj komendę 'rmate <plik\_do\_edycji>&#8217;. Pliczek powinien się automagicznie otworzyć w VSCode

W codziennej pracy odpalamy tylko punkty 3,4,5.

Plusy? Chyba widać. Da się ładnie edytować zdalnie zlokalizowane pliki 'on-line&#8217;.

Minusy? Jeszcze nie rozgryzłem jak odpalać zdalnie zabawki typu golint. Na razie walidacja kodu robi mi się w pełni lokalnie (a to mi się nie do końca podoba).

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2017/04/tunel_rmate.jpg