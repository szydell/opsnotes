---
title: '[Fedora] System76, Oryx Pro'
author: szydell
type: post
date: 2019-01-13T11:18:32+00:00
url: /post/2019/fedora-system76-oryx-pro/
categories:
  - Fedora
  - Linux
tags:
  - Fedora
  - oryx pro
  - system76

---
Firma <a rel="noreferrer noopener" aria-label="system76 (otwiera się na nowej zakładce)" href="https://system76.com/" target="_blank">system76</a> jest jednym z niewielu &#8222;producentów&#8221; sprzętu, dla których Linux jest pierwszym i jedynym wspieranym systemem operacyjnym. Niestety wymyślili sobie, że będą wspierać Ubuntu i opartego o niego POP!a (własnej produkcji).  
Osobiście wolę wynalazki RHEL&#8217;o podobne, także na laptopie/desktopie pracuję na Fedorze. Jak można się domyślać, na moim laptopie niektóre funkcjonalności nie działają out of box.  
Żeby rozwiązać problem zarządzania wentylatorami, kolorkami klawiatury itp. przygotowałem repozytorium z paczkami dla Fedory dostępne na moim koprze: <a rel="noreferrer noopener" aria-label="https://copr.fedorainfracloud.org/coprs/szydell/system76/ (otwiera się na nowej zakładce)" href="https://copr.fedorainfracloud.org/coprs/szydell/system76/" target="_blank">https://copr.fedorainfracloud.org/coprs/szydell/system76/</a>

Dodanie repozytorium do Fedory:

<pre class="wp-block-code"><code>sudo dnf install dnf-plugins-core
sudo dnf copr enable szydell/system76 </code></pre>

Instalacja i start:

<pre class="wp-block-code"><code>sudo dnf install system76-dkms system76-power system76-firmware
sudo systemctl enable system76-power system76-power-wake system76-firmware-daemon
sudo systemctl start system76-power system76-firmware-daemon</code></pre>

Do pełni szczęścia brakuje jeszcze paczki system76-driver. Zaktualizuję posta jak będzie dostępny.