---
title: UltraVNC – odświeżanie ekranu – potrzebny driver
author: szydell
type: post
date: 2013-10-28T10:28:38+00:00
url: /post/2013/ultravnc-odswiezanie-ekranu-potrzebny-driver/
categories:
  - VNC
tags:
  - odświeżanie
  - UltraVNC
  - video driver
  - VNC

---
Na jednej z używanych przeze mnie maszyn okazało się, że metoda opisana wcześniej w poście <a title="UltraVNC – problemy z odświeżaniem ekranu" href="https://marcin.szydelscy.pl/post/2013/ultravnc-problemy-z-odswiezaniem-ekranu/" target="_blank" rel="bookmark">UltraVNC – problemy z odświeżaniem ekranu</a> nie zadziałała wystarczająco dobrze.

Konieczne było dogranie po stronie serwera drivera video, o którym wspominałem na koniec.

Jak się za to zabrać? Przede wszystkim trzeba pobrać kolejny pakiet ze strony UltraVNC: <a href="http://www.uvnc.com/downloads/mirror-driver.html" target="_blank">http://www.uvnc.com/downloads/mirror-driver.html</a> lub w razie problemów ściągnąć [tę kopię.][1] <small>(wersja z 28.10.2013)</small>

Po zainstalowaniu pakietu (może być potrzebny reboot), należy skonfigurować samo UltraVNC, w tym celu w zakładce 'Screen Capture&#8217; sprawdzamy czy driver działa poprawnie 'Check the Mirror Driver&#8217;. Jeżeli tak, to włączamy go przez zaznaczenie opcji 'Use mirror driver&#8217;.

<figure id="attachment_78" aria-describedby="caption-attachment-78" style="width: 300px" class="wp-caption aligncenter">[<img loading="lazy" decoding="async" class="size-medium wp-image-78" alt="uvnc video driver konfiguracja" src="https://marcin.szydelscy.pl/wp-content/uploads/2013/10/uvnc_video_driver-300x283.jpg" width="300" height="283" />][2]<figcaption id="caption-attachment-78" class="wp-caption-text">UltraVNC video driver konfiguracja</figcaption></figure>

Teraz należy uruchomić serwer VNC ponownie i problem z odświeżaniem powinien się skończyć.

 [1]: https://marcin.szydelscy.pl/wp-content/uploads/2013/10/drivers.zip
 [2]: https://marcin.szydelscy.pl/wp-content/uploads/2013/10/uvnc_video_driver.jpg