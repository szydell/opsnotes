---
title: 'OpenVMS: Kasowanie katalogu z całą zawartością'
author: szydell
type: post
date: 2014-05-10T09:27:46+00:00
url: /post/2014/openvms-kasowanie-katalogu-z-cala-zawartoscia/
categories:
  - OpenVMS
tags:
  - Alpha
  - del
  - delete
  - OpenVMS
  - set

---
Niby banalna sprawa&#8230;

Najpierw wszysto w środku:

<pre>SET FILE/PROT=O:RWED [.KATALOG...]*.*;*
DELETE [.KATALOG...]*.*;*
</pre>

uwaga! powyższą czynność (delete) trzeba czasem wykonać wielokrotnie, aż skasuje się wszystkie pliki.

A teraz sam katalog:

<pre>SET FILE/PROT=O:RWED KATALOG.DIR
DELETE KATALOG.DIR;
</pre>