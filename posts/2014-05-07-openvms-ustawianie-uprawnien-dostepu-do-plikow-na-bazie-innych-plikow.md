---
title: 'OpenVMS: Ustawianie uprawnień dostępu do plików na bazie innych plików'
author: szydell
type: post
date: 2014-05-07T18:28:23+00:00
url: /post/2014/openvms-ustawianie-uprawnien-dostepu-do-plikow-na-bazie-innych-plikow/
categories:
  - OpenVMS
tags:
  - Alpha
  - like
  - OpenVMS
  - sec
  - set

---
<pre>set sec /like=(name=DISK$nazwa:[katalog]plik.txt) DISK$innanazwa:[innykatalog]innyplik.txt
</pre>

Jak w temacie. W ten sposób można ustawić, żeby innyplik.txt miał identyczne prawa dostępu jak plik.txt.