---
title: Firefox, która zakładka żre CPU? Monitoruj wydajność Firefoxa
author: szydell
type: post
date: 2020-06-15T08:07:00+00:00
url: /post/2020/firefox-ktora-zakladka-zre-cpu/
featured_image: /wp-content/uploads/2020/06/firefox-logo.png
categories:
  - Aplikacje
tags:
  - firefox
  - performance
  - wydajność

---
Trywialna sprawa. Bazylion zakładek. Top pokazuje, że 'Web Content&#8217; obciąża cały core. U mnie wydajność Firefoxa ściśle przekłada się na prędkość działania wentylatorów, także to ważne pytanie &#8211; jak namierzyć która? 

Okazuje się, ze nowsze wersje <a href="https://www.mozilla.org/pl/firefox/products/" target="_blank" rel="noreferrer noopener">Firefoxa</a> mają Manager zadań. Żeby go uruchomić, wystarczy w pasku na URL wpisać:

<pre class="wp-block-preformatted">about:performance </pre>