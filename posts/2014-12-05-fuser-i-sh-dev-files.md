---
title: 'fuser i sh dev  /files'
author: szydell
type: post
date: 2014-12-05T14:42:56+00:00
url: /post/2014/fuser-i-sh-dev-files/
categories:
  - Linux
  - OpenVMS
tags:
  - fuser
  - gt.m
  - sh dev

---
2 narzędzia służące do tego samego, jedno z linuxa drugie z openvmsa

Służą do wyświetlania kto (jaki proces) korzysta z konkretnego pliku.  
Niesamowicie przydatne w przypadku np. zarządzania bazy GT.M. Można w ten sposób sprawdzić czy ktoś przypadkiem nie pracuje na bazie.