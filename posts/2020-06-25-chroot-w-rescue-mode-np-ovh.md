---
title: Chroot w rescue mode (np. OVH)
author: szydell
type: post
date: 2020-06-25T14:14:33+00:00
url: /post/2020/chroot-w-rescue-mode-np-ovh/
categories:
  - Aplikacje
  - Linux
tags:
  - CentOS
  - chroot
  - rescue

---
Miałem ostatnio sytuację z niedziałającym VPS&#8217;em w OVH. Jedyna opcja to &#8211; rescue mode i dostanie się 'z boku&#8217; do systemu.

Jak to zrobić dobrze?

<pre class="wp-block-preformatted"># mkdir -p /mnt/myos
# mount <span class="has-inline-color has-red-color">/dev/sdXX</span> /mnt/myos
# mount -t proc proc /mnt/myos/proc
# mount --rbind /dev /mnt/myos/dev
# mount --rbind /sys /mnt/myos/sys
# chroot /mnt/myos /bin/bash</pre>

<p class="wp-block-coblocks-highlight">
  <mark class="wp-block-coblocks-highlight__content">W moim przypadku dyskiem vps&#8217;a jest /dev/sdb1. Jeżeli nie wiesz jaki dysk powinieneś podać, użyj komend 'lsblk&#8217; i 'blkid&#8217;.</mark>
</p>

Możemy teraz wprowadzać zmiany mniej więcej tak samo jakby nasz system działał. 

Jeżeli używamy selinuxa i wprowadzaliśmy jakieś zmiany, dobrze jest na koniec prac wykonać:

<pre class="wp-block-preformatted"># touch /.autorelabel</pre>

wymusi to przebudowanie etykiet selinuxa dla wszystkich plików.

Na koniec ctrl-d, żeby się wylogować z chroota i restart do normalnego trybu. Trzymam kciuki za poprawność wprowadzonych zmian. 🙂