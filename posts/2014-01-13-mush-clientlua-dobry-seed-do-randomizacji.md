---
title: 'MUSH Client/Lua: dobry seed do randomizacji'
author: szydell
type: post
date: 2014-01-13T22:31:39+00:00
url: /post/2014/mush-clientlua-dobry-seed-do-randomizacji/
categories:
  - MUSH Client
tags:
  - Lua
  - math.randomseed
  - MUSH Client
  - random
  - seed

---
Lua ma tragicznie mało precyzyjny os.timer() &#8211; działa z dokładnością do jednej sekundy. Jak na generator randoma&#8230; słabo.

Po długich poszukiwaniach znalazłem jednak rozwiązanie alternatywne:

<pre>math.randomseed(tonumber(string.sub(utils.timer(),9)))
</pre>

Uwaga!  
To rozwiązanie działa tylko pod MUSH Clientem. Niestety 'goła Lua&#8217; nie ma tak skonstruowanej biblioteki utils.