---
title: 'BASH: Proste, ustandaryzowane logowanie z poziomu skryptów (przy użyciu sysloga)'
author: szydell
type: post
date: 2014-04-25T07:34:16+00:00
url: /post/2014/bash-proste-ustandaryzowane-logowanie-z-poziomu-skryptow-przy-uzyciu-sysloga/
categories:
  - bash
  - Linux
tags:
  - bash
  - logger
  - syslog

---
Do tego niebywale trudnego zadania, służy aplikacja **logger**.

<pre>usage: logger [-is] [-f file] [-p pri] [-t tag] [-u socket] [ message ... ]
</pre>

Jak widać składnia jest bardzo trudna, także przykłady

<pre>[szydell@centos ~]$ logger "Się popsuło"
[root@centos ~]# cat /var/log/messages
...
Apr 25 09:01:42 centos szydell: Się popsuło
</pre>

-f /katalog/nazwapliku.log &#8211; logowanie do konkretnego pliku (zadbaj o uprawnienia)  
-t SIAKISTAG &#8211; zamiast logowania jaki user uruchomił, zaloguje taga