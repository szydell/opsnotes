---
title: 'fedora 19: instalacja driverów nvidia'
author: szydell
type: post
date: 2013-12-12T13:48:06+00:00
url: /post/2013/fedora-19-instalacja-driverow-nvidia/
categories:
  - Linux
tags:
  - Fedora
  - Fedora 19
  - gdm
  - główny monitor
  - gnome
  - gnome 3.8
  - nvidia
  - nvidia drivers
  - pierwszy monitor
  - primary display
  - twinview

---
Ostatnio postanowiłem zbudować sobie desktopa na fedorze. Padło na wersję 19 z gnomem. Standardowo jak to zwykle bywa pod linuxem, bez problemów się nie obeszło 😉

Po zainstalowaniu 'gołego systemu&#8217; trzeba go co nie co dopracować, żeby na początek przynajmniej grafika działała na driverach nvidii.

Posiadam GeForce 650Ti, która działa obecnie na  driverach rodziny 331. Instaluję wersję 331.20. Jakby co tutaj dają listę działających z tą wersją driverów kart: <ftp://download.nvidia.com/XFree86/Linux-x86_64/331.20/README/supportedchips.html>

Po kolei:

<pre>yum update kernel* selinux-policy*
reboot
yum localinstall --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
yum localinstall --nogpgcheck http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
yum install akmod-nvidia xorg-x11-drv-nvidia-libs
reboot</pre>

Tutaj linux poprawnie wykrył, że mam dwa monitory&#8230; i niepoprawnie ustawił ich kolejność.

Można sobie pokombinować z 'ustawieniami&#8217; i innymi cudami, ale najprościej wyedytować plik:

<pre>~/.config/monitors.xml</pre>

a następnie skopiować go do lokalizacji:

<pre>/var/lib/gdm/.config/</pre>

Pierwszy edit poprawi nam wyświetlanie po uruchomieniu gnoma, a drugi spowoduje, że gdm (czyli ekran logowania) też wróci na poprawne miejsce.

dodatkowo w tym momencie postanowiłem prewencyjnie zrobić update całego systemu:

<pre>yum check-update
yum update

reset</pre>

Co ciekawe po reboocie poprawnie uruchomił się konfigurator pierwszego uruchomienia gnome. Świeżo zainstalowany system ma tę funkcjonalność schrzanioną 🙂

Na koniec:

<pre>yum install vdpauinfo libva-vdpau-driver libva-utils</pre>

Gotowe.