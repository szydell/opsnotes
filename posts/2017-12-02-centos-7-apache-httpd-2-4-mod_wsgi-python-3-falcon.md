---
title: 'Centos 7 + apache httpd >=2.4 + mod_wsgi + Python 3 + falcon'
author: szydell
type: post
date: 2017-12-02T14:40:46+00:00
url: /post/2017/centos-7-apache-httpd-2-4-mod_wsgi-python-3-falcon/
categories:
  - apache httpd
  - python
tags:
  - falcon
  - mod_wsgi

---
Odpalałem ostatnio prostą aplikacyjkę napisaną w pythonie 3 z wykorzystaniem frameworka falcon. Troszkę było kombinowania.  
Nie poruszam tutaj kompletnie kwestii uprawnień, selinuxa itd. Zakładam też działające apache httpd i skonfigurowanego vhosta.

  1. Dodaj repozytorium IUS do Centosa 7  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">yum install https://centos7.iuscommunity.org/ius-release.rpm</pre>

  2. Zainstaluj Python 3.6, mod_wsgi i pip.  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">yum install python36u-mod_wsgi python36u-pip</pre>

  3. Stwórz środowisko wirtualne 'app1&#8242; potrzebne do uruchomienia aplikacji. Ścieżka przykładowa.  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">sudo mkdir -p /srv/pythonenvs
cd /srv/pythonenvs
python3.6 -m venv app1</pre>

  4. Aktywuj venv dla swojej sesji  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">source app1/bin/activate</pre>

  5. Zainstaluj falcona i wszystkie inne zależności  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">(app1)$ pip install falcon</pre>

  6. Zakładam, że aplikację masz w katalogu /srv/app1/www, a 'entry point&#8217; to main.py. Bardzo ważne! Aplikacja musi się przedstawiać serwerowi wsgi jako 'application&#8217;. To znaczy, że w kodzie falcon musi być odpalony w następujący sposób:  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">api = application = falcon.API()</pre>

  7. Dodaj do konfiguracji vhosta:  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">WSGIDaemonProcess app1 python-path=/srv/app1/www/ python-home=/srv/pythonenvs/app1
WSGIProcessGroup app1
WSGIScriptAlias / /srv/app1/www/main.py
&lt;Directory /srv/app1/www/&gt;
Require all granted
&lt;/Directory&gt;</pre>

  8. Teraz tylko restart httpd:  
    <?prettify linenums=true?></p> 
    
    <pre class="prettyprint">systemctl restart httpd</pre>