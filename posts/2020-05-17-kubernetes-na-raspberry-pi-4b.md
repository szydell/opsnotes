---
title: Kubernetes na Raspberry Pi 4b
author: szydell
type: post
date: 2020-05-17T07:07:00+00:00
url: /post/2020/kubernetes-na-raspberry-pi-4b/
featured_image: /wp-content/uploads/2020/05/kubernetes-logo-604x270.jpg
categories:
  - Aplikacje
  - DevOps
  - Docker
  - HypriotOS
  - Kubernetes
  - Linux
  - Raspberry Pi
tags:
  - docker
  - flannel
  - flash
  - kubeadm
  - kubectl
  - rpi4

---
<p class="has-drop-cap">
  Notatki krok po kroku &#8211; jak na jednym i więcej Raspbery Pi 4b zainstalować HypriotOS, skonfigurować Dockera i ostatecznie uruchomić klaster Kubernetes.
</p>

Do flashowania kart będę używał komputera działającego pod Fedorą, ale kroki są na tyle uniwersalne, że można je spokojnie wykonać na większości innych dystrybucji. 

## 1. HypriotOS {.wp-block-heading}

Czemu akurat HypriotOS, a nie Raspbian/Ubuntu/Windows IoT/etc? Z lenistwa 😉  
HypriotOS to Debian okrojony gdzie trzeba, a rozbudowany w kierunku bycia hostem dla kontenerów. Dodatkowo jest optymalizowany pod RPi.  
Czyli jest dokładnie tym, czego potrzebuję.

### 1a. Program do flashowania {.wp-block-heading}

HypriotOS stworzyło skrypt, który ułatwia flashowanie systemu na kartę sd. Wygodne to to i działa.

<pre class="wp-block-preformatted"><a href="https://github.com/hypriot/flash/releases" target="_blank" rel="noreferrer noopener">https://github.com/hypriot/flash/releases</a>.</pre>

Skrypt wykorzystuje hdparm, pv, unzip i curla, także dla świętego spokoju można zacząć od próby doinstalowania braków. Na Fedorze wygląda to tak:

<pre class="wp-block-code"><code>sudo dnf install pv curl unzip hdparm</code></pre>

Teraz ściągamy skrypt. Link do wersji 2.7.0 &#8211; przetestowanej i działającej.  
Dajemy prawa zapisu i opcjonalnie wrzucamy sobie do /usr/local/bin.

<pre class="wp-block-code"><code>curl -LO https://github.com/hypriot/flash/releases/download/2.7.0/flash
chmod +x flash
sudo mv flash /usr/local/bin/flash
</code></pre>

### 1b. Wstępna konfiguracja {.wp-block-heading}

Skrypt 'flash&#8217; daje możliwość sparametryzowania HypriotOS według własnych potrzeb. Część ustawień można podać używając parametrów, część wymaga stworzenia pliku konfiguracyjnego. Można go stworzyć na podstawie <a rel="noreferrer noopener" href="https://github.com/hypriot/flash/tree/master/sample" target="_blank">dostępnych sampli</a>.  


Mój plik konfiguracyjny (_myhypriot.yml_) wygląda następująco:

<div class="wp-block-coblocks-gist">
  <noscript>
    <a href="https://gist.github.com/szydell/c2d2053a871c9584592effbb44f704bd">Zobacz ten gist w serwisie GitHub</a>
  </noscript>
</div>

Uwaga! Jeżeli skorzystasz z konfiguracji powyżej Malinka dostanie adres z DHCP. Jeżeli adres powinien być statyczny, sprawdź składnię w <a href="https://github.com/hypriot/flash/tree/master/sample" target="_blank" rel="noreferrer noopener">przykładowych konfigach</a> i wprowadź poprawki w myhypriot.yml.

### 1c. Flashowanie {.wp-block-heading}

Posiadam trzy Raspbery Pi 4. Każde chcę mieć z innym hostname, ale z tym samym plikiem konfiguracyjnym.

W chwili pisania tej notatki, HypriotOS dostępny jest w wersji 1.12.0. Nowsze wersje można znaleźć na stronie <a href="https://github.com/hypriot/image-builder-rpi/releases" target="_blank" rel="noreferrer noopener">https://github.com/hypriot/image-builder-rpi/releases</a>.  


<pre class="wp-block-preformatted"><strong>Trzykrotnie wkładam więc kartę microsd do laptopa i uruchamiam jako root (za każdym razem z innym hostname):</strong>

<code>flash --hostname &lt;strong>berta&lt;/strong> &lt;a rel="noreferrer noopener" href="https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.0/hypriotos-rpi-v1.12.0.img.zip" target="_blank">https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.0/hypriotos-rpi-v1.12.0.img.zip&lt;/a></code></pre>

<div class="wp-block-coblocks-accordion">
  <div class="wp-block-coblocks-accordion-item">
    <details><summary class="wp-block-coblocks-accordion-item__title">Log z przykładowego wykonania (<em>rozwiń</em>)</summary>
    
    <div class="wp-block-coblocks-accordion-item__content">
      <pre class="wp-block-code"><code># flash --hostname test --userdata myhypriot.yml https://github.com/hypriot/image-builder-rpi/releases/download/v1.12.0/hypriotos-rpi-v1.12.0.img.zip           
Using cached image /tmp/hypriotos-rpi-v1.12.0.img

Is /dev/mmcblk0 correct? y
Unmounting /dev/mmcblk0 ...
Flashing /tmp/hypriotos-rpi-v1.12.0.img to /dev/mmcblk0 ...
1,27GiB 0:00:00 &#91;2,79GiB/s] &#91;============================================================================================================================================================================================&gt;] 100%            
0+20800 records in
0+20800 records out
1363148800 bytes (1,4 GB, 1,3 GiB) copied, 640,081 s, 2,1 MB/s
Waiting for device /dev/mmcblk0

/dev/mmcblk0:
 re-reading partition table
Mounting Disk
Mounting /dev/mmcblk0 to customize...
total 52604
drwxr-xr-x. 3 root root    16384 sty  1  1970 .
drwxr-xr-x. 3 root root       60 maj  2 22:19 ..
-rwxr-xr-x. 1 root root    23966 wrz 20  2019 bcm2708-rpi-b.dtb
-rwxr-xr-x. 1 root root    24229 wrz 20  2019 bcm2708-rpi-b-plus.dtb
-rwxr-xr-x. 1 root root    23747 wrz 20  2019 bcm2708-rpi-cm.dtb
-rwxr-xr-x. 1 root root    23671 lip  8  2019 bcm2708-rpi-zero.dtb
-rwxr-xr-x. 1 root root    24407 lip  8  2019 bcm2708-rpi-zero-w.dtb
-rwxr-xr-x. 1 root root    25293 lip  8  2019 bcm2709-rpi-2-b.dtb
-rwxr-xr-x. 1 root root    25422 wrz 25  2019 bcm2710-rpi-2-b.dtb
-rwxr-xr-x. 1 root root    26463 lip  8  2019 bcm2710-rpi-3-b.dtb
-rwxr-xr-x. 1 root root    27082 lip  8  2019 bcm2710-rpi-3-b-plus.dtb
-rwxr-xr-x. 1 root root    25277 wrz 20  2019 bcm2710-rpi-cm3.dtb
-rwxr-xr-x. 1 root root    40559 wrz 17  2019 bcm2711-rpi-4-b.dtb
-rwxr-xr-x. 1 root root    52296 wrz 25  2019 bootcode.bin
-rwxr-xr-x. 1 root root      246 sty  7 19:31 cmdline.txt
-rwxr-xr-x. 1 root root      203 sty  7 19:31 config.txt
-rwxr-xr-x. 1 root root    18693 cze 24  2019 COPYING.linux
-rwxr-xr-x. 1 root root       20 sty  7 19:30 fake-hwclock.data
-rwxr-xr-x. 1 root root     3073 wrz 25  2019 fixup4cd.dat
-rwxr-xr-x. 1 root root     6167 wrz 25  2019 fixup4.dat
-rwxr-xr-x. 1 root root     9247 wrz 25  2019 fixup4db.dat
-rwxr-xr-x. 1 root root     9249 wrz 25  2019 fixup4x.dat
-rwxr-xr-x. 1 root root     2657 wrz 25  2019 fixup_cd.dat
-rwxr-xr-x. 1 root root     6736 wrz 25  2019 fixup.dat
-rwxr-xr-x. 1 root root     9808 wrz 25  2019 fixup_db.dat
-rwxr-xr-x. 1 root root     9810 wrz 25  2019 fixup_x.dat
-rwxr-xr-x. 1 root root  5310624 wrz 25  2019 kernel7.img
-rwxr-xr-x. 1 root root  5628040 wrz 25  2019 kernel7l.img
-rwxr-xr-x. 1 root root 13230592 wrz 25  2019 kernel8.img
-rwxr-xr-x. 1 root root  5029176 wrz 25  2019 kernel.img
-rwxr-xr-x. 1 root root     1494 cze 24  2019 LICENCE.broadcom
-rwxr-xr-x. 1 root root       23 sty  7 19:25 meta-data
-rwxr-xr-x. 1 root root      365 sty  7 19:35 os-release
drwxr-xr-x. 2 root root    16384 sty  7 19:30 overlays
-rwxr-xr-x. 1 root root   770816 wrz 25  2019 start4cd.elf
-rwxr-xr-x. 1 root root  4733128 wrz 25  2019 start4db.elf
-rwxr-xr-x. 1 root root  2769540 wrz 25  2019 start4.elf
-rwxr-xr-x. 1 root root  3683816 wrz 25  2019 start4x.elf
-rwxr-xr-x. 1 root root   685668 wrz 25  2019 start_cd.elf
-rwxr-xr-x. 1 root root  4854728 wrz 25  2019 start_db.elf
-rwxr-xr-x. 1 root root  2877988 wrz 25  2019 start.elf
-rwxr-xr-x. 1 root root  3792232 wrz 25  2019 start_x.elf
-rwxr-xr-x. 1 root root     1713 sty  7 19:25 user-data
Copying cloud-init myhypriot.yml to /tmp/0/mnt.477635/user-data ...
Set hostname=test
Unmounting /dev/mmcblk0 ...
Finished.
</code></pre>
    </div></details>
  </div>
</div>

Przekładamy kartę do RPi, podpinamy do sieci i odpalamy. Po kilku minutach potrzebnych na pierwotną autokonfigurację, HypriotOS powinien być dostępny po ssh.

## 2. OS & Docker {.wp-block-heading}

<p class="has-small-font-size">
  <strong>Kroki do wykonania na wszystkich maszynach!</strong>
</p>

Zaktualizuj i zrestartuj system

<pre class="wp-block-preformatted">$ sudo -i
# apt update && apt upgrade && reboot</pre>

Popraw konfigurację Dockera tak, aby:

  * do zarządzania cgroup&#8217;ami używał systemd
  * logi tworzył w formacie json
  * zajmował do 100MiB na logi
  * używał sterownika storage w wersji overlay2

<pre class="wp-block-preformatted">$ sudo -i
# <code>cat &gt; /etc/docker/daemon.json &lt;&lt;EOF</code>
<code>{</code>
  <code>"exec-opts": ["native.cgroupdriver=systemd"],</code>
  <code>"log-driver": "json-file",</code>
  <code>"log-opts": { "max-size": "100m" },</code>
  <code>"storage-driver": "overlay2"</code>
<code>}</code>
<code>EOF</code></pre>

(opcjonalnie) Tworzymy katalog na parametry serwisu Docker i restartujemy go

<pre class="wp-block-preformatted"># <code>mkdir -p /etc/systemd/system/docker.service.d</code>
# <code>systemctl daemon-reload</code>
# <code>systemctl restart docker</code></pre>

Ustawiamy parametry kernela pozwalające iptables poprawnie widzieć ruch w bridge&#8217;ach.

<pre class="wp-block-preformatted">#<code> cat &lt;&lt;EOF | tee /etc/sysctl.d/k8s.conf</code>
<code>net.bridge.bridge-nf-call-ip6tables = 1</code>
<code>net.bridge.bridge-nf-call-iptables = 1</code>
<code>EOF</code>
# <code>sysctl --system</code></pre>

Tworzymy katalog /etc/cnt/net.d. Wymagany jest przez kubelet.service, a niestety instalator go nie przygotowuje. Brak katalogu skutkuje błędami NetworkPluginNotReady.

<pre class="wp-block-preformatted"># mkdir -p /etc/cnt/net.d</pre>

## 3. Instalacja kubernetesa {.wp-block-heading}

<p class="has-small-font-size">
  <strong>Kroki do wykonania na wszystkich maszynach!</strong>
</p>

Dodaj oficjalne repozytorium Kubernetes

<pre class="wp-block-preformatted"># <code>curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -</code>
# echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" &gt; /etc/apt/sources.list.d/kubernetes.list
# apt update</pre>

Zainstaluj!

<pre class="wp-block-preformatted"># <code>apt install -y kubeadm</code></pre>

4. To ja tu rządzę!

Wybierz jedną z maszyn do zarządzania klastrem. Wydaj na niej komendę:

<pre class="wp-block-preformatted"># <code>kubeadm init --pod-network-cidr 10.244.0.0/16</code></pre>

<div class="wp-block-coblocks-accordion">
  <div class="wp-block-coblocks-accordion-item">
    <details><summary class="wp-block-coblocks-accordion-item__title">Log z wykonania (rozwiń)</summary>
    
    <div class="wp-block-coblocks-accordion-item__content">
      <pre class="wp-block-preformatted"><strong># kubeadm init --pod-network-cidr 10.244.0.0/16</strong>
W0503 10:39:57.299688 32439 configset.go:202] WARNING: kubeadm cannot validate component configs for API groups [kubelet.config.k8s.io kubeproxy.config.k8s.io]
[init] Using Kubernetes version: v1.18.2
[preflight] Running pre-flight checks
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Starting the kubelet
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [elsa kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 192.168.88.16]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [elsa localhost] and IPs [192.168.88.16 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [elsa localhost] and IPs [192.168.88.16 127.0.0.1 ::1]
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
W0503 10:42:11.343155 32439 manifests.go:225] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[control-plane] Creating static Pod manifest for "kube-scheduler"
W0503 10:42:11.346581 32439 manifests.go:225] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 38.015489 seconds
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.18" in namespace kube-system with the configuration for the kubelets in the cluster
[upload-certs] Skipping phase. Please see --upload-certs
[mark-control-plane] Marking the node elsa as control-plane by adding the label "node-role.kubernetes.io/master=''"
[mark-control-plane] Marking the node elsa as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: zuqc0t.xyq5i6lzbf9jarlb
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstrap-token] Creating the "cluster-info" ConfigMap in the "kube-public" namespace
[kubelet-finalize] Updating "/etc/kubernetes/kubelet.conf" to point to a rotatable kubelet client certificate and key
[kubelet-check] Initial timeout of 40s passed.
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy
Your Kubernetes control-plane has initialized successfully!
To start using your cluster, you need to run the following as a regular user:
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
https://kubernetes.io/docs/concepts/cluster-administration/addons/
Then you can join any number of worker nodes by running the following on each as root:
kubeadm join 192.168.88.16:6443 --token asdfgh.12qaz2wsx3edc4rf \
--discovery-token-ca-cert-hash sha256:1qaz2wsx3edc4rfv5tgb6yhn7ujm8ikk9oll0ppp1qaz2wsx3edc4rfv5t</pre>
    </div></details>
  </div>
</div>

Na koniec komendy inicjalizacyjnej dostajemy zestaw zaleceń do zrealizowania:

  1. Wykonaj na userze na którym będziesz standardowo pracować poniższe komendy. Zarządzanie kubernetesem z roota jest mało sexi.

<pre class="wp-block-preformatted">$ mkdir -p $HOME/.kube
$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config</pre>

2. Skopiuj sobie gdzieś na bok komendę zawierającą token i klucz. **Nie wykonuj jej jeszcze na workerach!** Chodzi o coś wyglądającego w ten sposób:

<pre class="wp-block-preformatted">kubeadm join 192.168.88.16:6443 --token asdfgh.12qaz2wsx3edc4rf \<br />--discovery-token-ca-cert-hash sha256:1qaz2wsx3edc4rfv5tgb6yhn7ujm8ikk9oll0ppp1qaz2wsx3edc4rfv5t</pre>

Na naszej głównej maszynie przenosimy się na usera 'zarządzającego&#8217;. Czyli tego dla którego przygotowaliśmy punkt 1 powyżej.

## 3. Flannel {.wp-block-heading}

Czyli wirtualna sieć, która udostępnia odpowiednie podsieci każdemu z hostów klastra.  
  
Instalacja:

<pre class="wp-block-preformatted">$ <code>kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml</code></pre>

<div class="wp-block-coblocks-accordion">
  <div class="wp-block-coblocks-accordion-item">
    <details><summary class="wp-block-coblocks-accordion-item__title">Log z wykonania (rozwiń)</summary>
    
    <div class="wp-block-coblocks-accordion-item__content">
      <p>
        $ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml<br />podsecuritypolicy.policy/psp.flannel.unprivileged created<br />clusterrole.rbac.authorization.k8s.io/flannel configured<br />clusterrolebinding.rbac.authorization.k8s.io/flannel unchanged<br />serviceaccount/flannel created<br />configmap/kube-flannel-cfg created<br />daemonset.apps/kube-flannel-ds-amd64 created<br />daemonset.apps/kube-flannel-ds-arm64 created<br />daemonset.apps/kube-flannel-ds-arm created<br />daemonset.apps/kube-flannel-ds-ppc64le created<br />daemonset.apps/kube-flannel-ds-s390x created
      </p>
      
      <p>
      </p>
    </div></details>
  </div>
</div>

## 4. Dołącz hosty do klastra {.wp-block-heading}

(opcjonalnie) Zezwól na swojej głównej maszynie na uruchamianie na niej PODów.  
Standardowo, z powodów bezpieczeństwa, host na którym odpalamy kubeadm init, jest wyłączony z klastra. Służy tylko do jego zarządzania. W warunkach domowego laba, to często marnotrawstwo zasobów.  
**Uwaga! Jeżeli robisz ten tutorial na pojedynczej maszynie, to poniższa komenda nie jest opcjonalna.**

<pre class="wp-block-preformatted"><code>kubectl taint nodes --all node-role.kubernetes.io/master-</code></pre>

<blockquote class="wp-block-quote">
  <p>
    Nie zanotowałeś sobie tokena?<br />Minęło ponad 24h od uruchomienia komendy kubeadm init? To nic strasznego. Skorzystaj z instrukcji we wpisie o <a rel="noreferrer noopener" href="https://marcin.szydelscy.pl/post/2020/kubernetes-dolaczanie-nowego-hosta-i-zarzadzanie-tokenami/" target="_blank">zarządzaniu tokenami w kubernetesie</a>.
  </p>
</blockquote>

Uruchom na wszystkich workerach komendę wymusząjącą dołączenie do klastra:

<pre class="wp-block-preformatted"># <code>kubeadm join --token &lt;token&gt; &lt;control-plane-host&gt;:&lt;control-plane-port&gt; --discovery-token-ca-cert-hash sha256:&lt;hash&gt;</code></pre>

<div class="wp-block-coblocks-accordion">
  <div class="wp-block-coblocks-accordion-item">
    <details><summary class="wp-block-coblocks-accordion-item__title">Log z przykładowego uruchomienia (rozwiń)</summary>
    
    <div class="wp-block-coblocks-accordion-item__content">
      <pre class="wp-block-preformatted">kubeadm join 192.168.88.16:6443 --token 3fs9sd.z4di8tmrxdskitl4 --discovery-token-ca-cert-hash sha256:c4d361fb1bd7a4511ab398b3e99d950dfac3b1235ae981628c79ac0c5110c156
W0516 14:24:09.006377 21168 join.go:346] [preflight] WARNING: JoinControlPane.controlPlane settings will be ignored when control-plane flag is not set.
[preflight] Running pre-flight checks
[preflight] Reading configuration from the cluster…
[preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.18" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Starting the kubelet
[kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap…
This node has joined the cluster:
Certificate signing request was sent to apiserver and a response was received.
The Kubelet was informed of the new secure connection details.
Run 'kubectl get nodes' on the control-plane to see this node join the cluster.</pre>
    </div></details>
  </div>
</div>

## 5. Weryfikacja {.wp-block-heading}

Zweryfikuj na głównej maszynie listę hostów w klastrze:

<pre class="wp-block-preformatted">$ kubectl get nodes</pre>

Przykładowo, może wyglądać to tak:

<pre class="wp-block-code"><code>$ kubectl get nodes
NAME     STATUS     ROLES    AGE     VERSION
berta    NotReady   &lt;none&gt;   4s      v1.18.2
elsa     Ready      master   13d     v1.18.2
ingrid   Ready      &lt;none&gt;   3m13s   v1.18.2

$ kubectl get nodes
NAME     STATUS   ROLES    AGE   VERSION
berta    Ready    &lt;none&gt;   12m   v1.18.2
elsa     Ready    master   13d   v1.18.2
ingrid   Ready    &lt;none&gt;   15m   v1.18.2

</code></pre>

Spróbujmy uruchomić na naszym klastrze mały serwis testowy

<pre class="wp-block-preformatted"><code>kubectl run hypriot --image=hypriot/rpi-busybox-httpd --port=80</code></pre>

Działa?

<pre class="wp-block-preformatted">$ kubectl get pods</pre>

<pre class="wp-block-code"><code>HypriotOS/armv7: szydell@elsa in ~
$ kubectl get pods
NAME      READY   STATUS    RESTARTS   AGE
hypriot   1/1     Running   0          96s
</code></pre>

Udostępnijmy teraz port 80 i sprawdźmy pod jakim ip, oraz na którym node kubernetes serwuje nasz serwis.

<pre class="wp-block-preformatted">$ kubectl expose po hypriot --port 80
$ kubectl get endpoints hypriot
$ kubectl describe pods/hypriot | grep Node:</pre>

Przykładowo:

<pre class="wp-block-preformatted">$ kubectl expose po hypriot --port 80
service/hypriot exposed
$ kubectl get endpoints hypriot
NAME ENDPOINTS AGE
hypriot 10.244.1.2:80 17s
$ kubectl describe pods/hypriot | grep Node:
Node: ingrid/192.168.88.15</pre>

Wchodzimy na hosta zwróconego przez ostatnią komendę. I jeżeli wszystko jest ok, powinniśmy móc lokalnie pobrać zawartość działającej strony:

<pre class="wp-block-code"><code>$ curl 10.244.1.2:80
&lt;html&gt;
&lt;head&gt;&lt;title&gt;Pi armed with Docker by Hypriot&lt;/title&gt;
  &lt;body style="width: 100%; background-color: black;"&gt;
    &lt;div id="main" style="margin: 100px auto 0 auto; width: 800px;"&gt;
      &lt;img src="pi_armed_with_docker.jpg" alt="pi armed with docker" style="width: 800px"&gt;
    &lt;/div&gt;
  &lt;/body&gt;
&lt;/html&gt;
</code></pre>

Jak widać wersja minimum działa. Wywalmy jeszcze dla sportu poda hypriot i sprawdzamy czy zniknął.

<pre class="wp-block-preformatted">$ kubectl delete pod hypriot --now
$ kubectl get pods</pre>

Kubernetes w wersji minimum jest zainstalowany i działa. Teraz dopiero zaczną się schody. 😉