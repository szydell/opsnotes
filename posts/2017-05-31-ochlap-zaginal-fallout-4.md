---
title: Ochłap zaginął, Fallout 4
author: szydell
type: post
date: 2017-05-31T21:30:35+00:00
url: /post/2017/ochlap-zaginal-fallout-4/
categories:
  - Aplikacje
tags:
  - dogmeat
  - fallout 4
  - ochłap

---
Może trochę nie na temat, ale&#8230; takie drobne ułatwienie dla graczy.

  1. Odpal konsolę (~)
  2. Wpisz 'prid 0001d162&#8242;, enter
  3. Wpisz 'moveto player&#8217;, enter
  4. Wyjdź z konsoli (~)

Ochłap powinien stać tuż obok 😉

Lista pozostałych towarzyszy, których można odzyskać w ten sam sposób:

  1. **Cait**: Human, Trigger Rush, Combat Zone, ID: **00079305**
  2. **Codsworth**: Mister Handy (Robot), Robot Sympathy, Sanctuary Hills, ID: **0001ca7d**
  3. **Curie**: Miss Nanny/Synth, Combat Medic, Vault81, ID: **00102249**
  4. **Paladin Danse:** Human, Know Your Enemy, Cambridge Police Station, ID: **0005de4d**
  5. **Deacon**: Human, Cloak & Dagger, Old North Church, ID: **00045ac9**
  6. **Dogmeat: **Dog, Attack Dog, Red Rocket Truck Stop, ID: **0001d162**
  7. **John Hancock:** Ghoul, Isodoped, Goodneighbor, **ID: 00022615**
  8. **Robert MacCready:** Human, Killshot, Goodneighbor, **ID: 0002a8a7**
  9. **Nick Valentine:** Synth, Close to Metal, Valut 114, ID: **00002f25**
 10. **Piper:** Human, Gift of Gab, Diamond City, ID: **0002f1f**
 11. **Preston Garvey:** Human, United We Stand, Concord, **ID: 0001a4d7**
 12. **Strong:** Super Mutant, Berserk, Trinity Tower, ID: **0003f2bb**
 13. **X6-88:** Synth, Shield Harmonics, The Institute, ID: **0002e210a**

&nbsp;

Skradzione stąd: <a href="https://www.gameskinny.com/03cw4/fallout-4-companion-list-perks-locations-ids/" target="_blank" rel="noopener noreferrer">https://www.gameskinny.com/03cw4/fallout-4-companion-list-perks-locations-ids/</a>