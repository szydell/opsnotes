---
title: Moduły nvidia nie ładują się
author: szydell
type: post
date: 2023-10-14T14:01:31+00:00
url: /post/2023/moduly-nvidia-nie-laduja-sie/
ao_post_optimize:
  - 'a:6:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";s:15:"ao_post_preload";s:0:"";}'
categories:
  - Aplikacje
  - Fedora
tags:
  - nvidia
  - nvidia drivers

---
Po reinstalacji systemu na Fedora 38, zainstalowałem jak zawsze drivery nvidii. Niestety tym razem okazało się, że po reboocie moduły się nie załadowały, a próba skorzystania z karty graficznej o większej mocy kończy się błędami, w których przewijało się słowo 'key&#8217;, oraz 

<pre class="wp-block-code"><code>X Error of failed request:  BadValue (integer parameter out of range for operation)</code></pre>

Przyczyna? Nieoczywista. Włączyłem secure boot, a moduły kernela nie były podpisane.

Weryfikacja:

<pre class="wp-block-code"><code>&#91;root@laPtak ~]# mokutil --sb-state
SecureBoot enabled</code></pre>

Naprawa:

<pre class="wp-block-code"><code>sudo dnf remove kmod-nvidia-*
sudo dnf install akmod-nvidia

</code></pre>

Następnie wykonaj instrukcję z pliku:

<pre class="wp-block-code"><code>/usr/share/doc/akmods/README.secureboot</code></pre>