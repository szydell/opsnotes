---
title: NGINX spdy invalid parameter
author: szydell
type: post
date: 2016-07-06T11:39:15+00:00
url: /post/2016/nginx_spdy_invalid_parameter/
categories:
  - nginx
tags:
  - http2
  - spdy

---
Przejrzałem sobie logi z nginxa i zastanowiły mnie poniższe wpisy pojawiające się przy wszystkich moich domenach z ssl.  
<?prettify linenums=true?>

<pre class="prettyprint">invalid parameter "spdy": ngx_http_spdy_module was superseded by ngx_http_v2_module in</pre>

Rozwiązanie oczywiście banalne, wszystkie linie:  
<?prettify linenums=true?>

<pre class="prettyprint">listen twojeip:443 ssl spdy;</pre>

zmieniamy na:  
<?prettify linenums=true?>

<pre class="prettyprint">listen twojeip:443 ssl http2;</pre>