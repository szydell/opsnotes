---
title: 'Lua: Operacje na tablicach'
author: szydell
type: post
date: 2013-09-26T11:29:00+00:00
url: /post/2013/lua-operacje-na-tablicach/
categories:
  - Lua
tags:
  - array
  - Lua
  - print_r
  - table
  - tablice

---
Kolejna ściągawka składniowa.

Zadeklarowanie tablicy:

<pre>NazwaZmiennej = {}</pre>

Zadeklarowanie tablicy z początkowymi danymi:

<pre>NazwaZmiennej = {"tekst 1", 3, InnaTablica= {"pole w innej tablicy","drugie pole w innej tablicy"},"kolejny napis w glownej tablicy"}</pre>

Push:

<pre>a = {}

    table.insert(a,"jakiś tekst")</pre>

&nbsp;

Pop:

<pre>table.remove(a,1)</pre>

Ile elementów ma tablica?

<pre>print(table.getn{10,2,4})          -- 3
    print(table.getn{10,2,nil})        -- 2
    print(table.getn{10,2,nil; n=3})   -- 3
    print(table.getn{n=1000})          -- 1000

    a = {}
    print(table.getn(a))               -- 0
    table.setn(a, 10000)
    print(table.getn(a))               -- 10000

    a = {n=10}
    print(table.getn(a))               -- 10
    table.setn(a, 10000)
    print(table.getn(a))               -- 10000</pre>

inną opcją jest użycie operatora #. Ma on jednak jedną wadę! Nie liczy, tylko wyświetla wartość najwyższego indeksu:

<pre>t = {"a", "b", "c"}
    = #t
    3</pre>

Pętla po parach w tablicy:

<pre>t = {"a", "b", [123]="foo", "c", name="bar", "d", "e"}
    for k,v in pairs(t) do print(k,v) end
    1       a
    2       b
    3       c
    4       d
    5       e
    123     foo
    name    bar</pre>

można też wykorzystać ipairs. Gwarantuje to, że zawsze otrzymamy rekordy w poprawnej kolejności. To rozwiązanie wyświetla jednak tabelę tylko po indeksach, nie po kluczach.

<pre>t = {"a", "b", "c"}
    for i, v in ipairs(t) do print(i, v) end
    1       a
    2       b
    3       c</pre>

<!--more-->

Funkcja do wyświetlania zawartości tablic w postaci zrozumiałej dla ludzi (implementacja print_r z php):

<pre>function print_r ( t ) 
        local print_r_cache={}
        local function sub_print_r(t,indent)
                if (print_r_cache[tostring(t)]) then
                        print(indent.."*"..tostring(t))
                else
                        print_r_cache[tostring(t)]=true
                        if (type(t)=="table") then
                                for pos,val in pairs(t) do
                                        if (type(val)=="table") then
                                                print(indent.."["..pos.."] = "..tostring(t).." {")
                                                sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                                                print(indent..string.rep(" ",string.len(pos)+6).."}")
                                        elseif (type(val)=="string") then
                                                print(indent.."["..pos..'] = "'..val..'"')
                                        else
                                                print(indent.."["..pos.."] = "..tostring(val))
                                        end
                                end
                        else
                                print(indent..tostring(t))
                        end
                end
        end
        if (type(t)=="table") then
                print(tostring(t).." {")
                sub_print_r(t,"  ")
                print("}")
        else
                sub_print_r(t,"  ")
        end
        print()
end</pre>

inne podejście do problemu:

<pre>function inspect(obj)
        if obj==nil then return "nil" end
        if obj==true then return "true" end
        if obj==false then return "false" end
        if type(obj)=="string" then return obj end
        local json = require "json"
        return json.encode({obj})
end

function debug(obj)
        print(inspect(obj))
end</pre>

Oba rozwiązania znalezione na: <a title="print_r in lua" href="http://developer.coronalabs.com/code/printr-implementation-dumps-everything-human-readable-text" target="_blank">http://developer.coronalabs.com/code/printr-implementation-dumps-everything-human-readable-text</a>