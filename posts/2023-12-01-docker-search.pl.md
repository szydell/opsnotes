---
title: docker/podman search
author: szydell
type: post
date: 2023-12-01T17:01:00+00:00
url: /post/2023/docker-search/
featured_image: /wp-content/uploads/2023/12/Podman-512x270.png
ao_post_optimize:
  - 'a:6:{s:16:"ao_post_optimize";s:2:"on";s:19:"ao_post_js_optimize";s:2:"on";s:20:"ao_post_css_optimize";s:2:"on";s:12:"ao_post_ccss";s:2:"on";s:16:"ao_post_lazyload";s:2:"on";s:15:"ao_post_preload";s:0:"";}'
categories:
  - Docker
  - Fedora
  - podman
  - Red Hat Enterprise Linux
tags:
  - docker
  - Fedora
  - kontenery
  - podman
  - ubi

---
docker search &#8211; przydatna funkcjonalność, do przeszukiwania repozytoriów.

Osobiście używam główne do znalezienia dostępnych kontenerów UBI od red hata. 🙂

<pre class="wp-block-code"><code>docker search registry.access.redhat.com/ubi</code></pre>

Inne używane czasami zastosowanie, to wyszukanie przygotowywanych przez fedoraproject kontenerów np. w danej wersji fedory:

<pre class="wp-block-code"><code>&#91;root@laPtak ~]# podman search registry.fedoraproject.org/f39
NAME                                                 DESCRIPTION
registry.fedoraproject.org/f39/flatpak-kde5-runtime  
registry.fedoraproject.org/f39/flatpak-kde5-sdk      
registry.fedoraproject.org/f39/flatpak-kde6-runtime  
registry.fedoraproject.org/f39/flatpak-kde6-sdk      
registry.fedoraproject.org/f39/flatpak-runtime       
registry.fedoraproject.org/f39/flatpak-sdk  </code></pre>

Tak znalezione kontenery można pobrać później korzystając z komendy docker/podman pull.
