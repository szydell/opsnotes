---
title: Synchronizacja czasu – Linux + ntp. Wersja nieprzesadzona.
author: szydell
type: post
date: 2014-06-04T12:05:26+00:00
url: /post/2014/synchronizacja-czasu-linux-wersja-nieprzesadzona/
categories:
  - Linux
tags:
  - CentOS
  - cron
  - crontab
  - ntpd
  - ntpdate

---
Wrzucałem już raz wpis o <a title="automatyczna synchronizacja czasu" href="https://marcin.szydelscy.pl/post/2014/automatyczna-synchronizacja-czasu-w-centos-6-5/" target="_blank" rel="noopener">automatycznej synchronizacji czasu</a>, ale po przemyśleniu tematu doszedłem do wniosku, że rozwiązanie tam zastosowane jest bez sensu. Czemu?

  1. Stawianie serwera ntpd tylko po to, żeby mieć aktualny czas na swojej maszynie, to totalny i typowy overkill. Rozwiązanie to oczywiście działa skutecznie, jest &#8222;zalecane w internetach&#8221;, ale jego nadmiarowość aż razi.
  2. Standardowa konfiguracja ntpd na CentOS (ale też pewnie w innych dystrybucjach) otwiera od razu port 123 na wszystkich możliwych interfejsach. Po co? No bo ntpd z założenia służy do serwowania czasu, a nie jego zwykłej synchronizacji.
  3. No właśnie. Po co nam serwer, skoro jedynie chcemy być klientami?

Rozwiązanie jest dużo prostsze. W CentOS 6.5, którego teraz głównie używam, standardowo zainstalowane jest cudo o nazwie ntpdate. Na 99,99999% serwerów, wystarczy synchronizacja czasu raz na dobę.

Chyba już widać co chcę przekazać. CRON + ntpdate rozwiążą nasz problem prosto i skutecznie.  
Co robimy?

W katalogu /etc/cron.daily zakładamy sobie plik ntpsynchro z prawami do uruchomienia:

<pre>[root@centos /]# touch /etc/cron.daily/ntpsynchro
[root@centos /]# chmod +x /etc/cron.daily/ntpsynchro
</pre>

Następnie wrzucamy do pliku tę zawartość (np. używając: nano /etc/cron.daily/ntpsynchro):  
**#!/bin/sh  
/usr/sbin/ntpdate pl.pool.ntp.org | logger 1> /dev/null 2>&1  
**  
Dobrze jest testowo i w celu pierwszej synchronizacji uruchomić skrypt:

<pre>/etc/cron.daily/ntpsynchro
</pre>

A następnie wyświetlić zawartość logu:

<pre>cat /var/log/messages | tail
</pre>

Poprawnie działający skrypt powinien zalogować nam coś w ten deseń:

<pre>Jun  4 14:04:52 centos root:  4 Jun 14:04:52 ntpdate[10409]: adjust time server 217.96.29.26 offset 0.000199 sec
</pre>

Co się właśnie stało? Nasz serwer, który jest w tej całej akcji KLIENTEM zapytał się serwera pl.pool.ntp.org o aktualny czas, zlogował odpowiedź i oczywiście jeżeli była taka potrzeba ustawił na naszej maszynie.  
Prawda, że takie rozwiązanie jest bardziej eleganckie, prostsze i bardziej bezpieczne? 🙂