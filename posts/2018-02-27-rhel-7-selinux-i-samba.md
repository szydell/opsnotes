---
title: '[RHEL 7] SELinux i Samba'
author: szydell
type: post
date: 2018-02-27T12:27:29+00:00
url: /post/2018/rhel-7-selinux-i-samba/
categories:
  - Aplikacje
  - Linux
tags:
  - centos 7
  - Red Hat Enterprise Linux 7
  - RHEL 7
  - Samba

---
Masz wizję postawienia samby? Nie działa? 🙂  
Jedna z ważniejszych rzeczy w nowych Red Hatach to ogarnięcie SELinuxa.  
Załóżmy, że chcesz udostępnić katalog /samba  
<?prettify linenums=true?>

<pre class="prettyprint">sudo semanage fcontext --add -t samba_share_t '/samba(/.*)?'
sudo restorecon -Rv /samba/</pre>

Banalne do zapamiętania, nie?