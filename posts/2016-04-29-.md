---
title: Docker cluster
author: szydell
type: post
date: -001-11-30T00:00:00+00:00
draft: true
url: /?p=331
categories:
  - Aplikacje

---
CoreOS

High-Availability Docker #1: https://youtu.be/wxUxtflalE4  
&#8211; Cluster computing &#8211; CoreOS is our solution here. CoreOS allows us to make an expandable cluster of computers so we can add more computers should our application need more power to scale. CoreOS also allows us to run services on any machine in the cluster and they can all communicate with each other.

High-Availability Docker #2: https://youtu.be/top0MlcKSw4  
&#8211; Scheduling: we shouldn&#8217;t manually choose which app runs on which machine &#8211; this idea doesn&#8217;t work at scale. Fleet will be our scheduler that decides where our apps/services run. If a CoreOS machine goes down, fleet will reschedule any services that it was running onto a new machine.

High-Availability Docker #3: https://youtu.be/1zJ8FfC-gmU  
&#8211; Service Registration  
Since we never know what ip address or port a service will be running on, we need to register that service so other services can find it and communicate with it. We&#8217;ll use Etcd and Flannel for this

High-Availability Docker #4: https://youtu.be/R39VRocQtrQ  
&#8211; Service Discovery:  
Now that services have registered, we can discover them and load balance between them with a generic docker nginx container

High-Availability Docker #5: https://youtu.be/4215szNHlC4  
&#8211; Public DNS: Once the app is working, we need a fixed way for the outside world to access it. We&#8217;ll use Nginx and Confd for this. Confd will watch Etcd for service registration and when services come/go, it will build a new configuration file for Nginx and reload Nginx.