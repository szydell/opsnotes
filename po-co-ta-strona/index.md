---
title: Po co ta strona?
author: szydell
type: page
date: 2013-09-24T19:28:43+00:00

---
Kto zapamięta jakiś ciąg czynności, który trzeba wykonywać raz na rok?  
Kto pamięta przypadkowe odkrycia, które chciałby zastosować za jakiś czas?

Ja nie. Stąd notatnik.

Czemu w formie bloga?  
Z prostoty dostępu z dowolnego miejsca, no i przede wszystkim z chęci walki z lenistwem. Mając świadomość, że ktoś jeszcze może te notatki przeczytać zmuszam się do ich lepszego tworzenia 😉